// @flow
import type { Record } from '@nativestudios/creace-core/typings/Record';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import React, { Component } from 'react';
import { VideoListItemFinished } from './VideoListItemFinished';
import { VideoListItemUnfinished } from './VideoListItemUnfinished';

type Props = {
    video: Video;
    template: Template;
    records: Array<Record>;
    onViewButtonPress: Function;
    onEditButtonPress: Function;
    onUploadButtonPress: Function;
    onDeleteButtonPress: Function;
};

export class VideoListItem extends Component<Props> {
    render() {
        const { video, template, records } = this.props;

        return video.state === 'finished' ?
            <VideoListItemFinished video={video}
                                   template={template}
                                   records={records}
                                   onViewButtonPress={this.props.onViewButtonPress}
                                   onEditButtonPress={this.props.onEditButtonPress}
                                   onDeleteButtonPress={this.props.onDeleteButtonPress}/> :
            <VideoListItemUnfinished video={video}
                                     template={template}
                                     records={records}
                                     onEditButtonPress={this.props.onEditButtonPress}
                                     onUploadButtonPress={this.props.onUploadButtonPress}
                                     onDeleteButtonPress={this.props.onDeleteButtonPress}/>;
    }
}
