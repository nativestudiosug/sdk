// @flow
import type { Category } from '@nativestudios/creace-core/typings/Category';
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { CategoryListItem } from './CategoryListItem';

type Props = {
    categories: Array<Category>;
    onCategoryPress: Function;
};

export class CategoryList extends Component<Props> {
    _keyExtractor(item) {
        return item.id.toString();
    }

    renderItem(data: { item: Category }) {
        return <CategoryListItem category={data.item} templates={this.props.templates}
                                 onPress={this.props.onCategoryPress}/>;
    }

    render() {
        const { categories } = this.props;

        categories.sort((a, b) => b.priority - a.priority);

        return (
            <FlatList keyExtractor={this._keyExtractor} data={categories}
                      renderItem={this.renderItem.bind(this)}/>
        );
    }
}
