// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import React from 'react';
import { ImageBackground, Text, TouchableHighlight } from 'react-native';
import imageCacheHoc from 'react-native-image-cache-hoc';

type Props = {
    video: Video;
    onViewButtonPress: Function;
    onEditButtonPress: Function;
    onDeleteButtonPress: Function;
};

export class VideoListItemFinished extends ContextualComponent<Props> {
    render() {
        const { video } = this.props;
        const CachableImage = imageCacheHoc(ImageBackground);
        const { createdAt, container, templateName, image } = this.theme.videoListItemTheme;

        return (
            <TouchableHighlight onPress={() => this.props.onViewButtonPress(video)}>
                <CachableImage style={[container, image, { aspectRatio: 1280 / 720 }]}
                               resizeMode="cover"
                               source={{ uri: video.preview_image }}>
                    <Text style={createdAt}>
                        {video.created_at}
                    </Text>
                    <Text style={templateName}>
                        {video.template.name}
                    </Text>
                </CachableImage>
            </TouchableHighlight>
        );
    }
}
