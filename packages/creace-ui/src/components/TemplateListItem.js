// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import React from 'react';
import { ImageBackground, Text, TouchableHighlight, View } from 'react-native';
import imageCacheHoc from 'react-native-image-cache-hoc';

type Props = {
    template: Template;
    onPress: Function;
};

export class TemplateListItem extends ContextualComponent<Props> {
    render() {
        const { template } = this.props;
        const previewImage = template.preview_image || undefined;
        const CachableImage = imageCacheHoc(ImageBackground);
        const { image, text } = this.theme.templateListItemTheme;

        return (
            <View>
                <TouchableHighlight onPress={() => this.props.onPress(template)}>
                    <CachableImage style={[image]}
                                   source={{ uri: previewImage }}
                                   resizeMode="cover">
                        <Text style={[text]}>
                            {template.name}
                        </Text>
                    </CachableImage>
                </TouchableHighlight>
            </View>
        );
    }
}
