// @flow
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { RecordListItem } from './RecordListItem';

type Props = {
    instructions: Array<Instruction>;
    records: Array<Record>;
    onEditButtonPress: Function;
    onViewButtonPress: Function;
};

export class RecordList extends Component<Props> {
    _keyExtractor(item: Instruction) {
        return item.id.toString();
    }

    renderItem(data: { item: Instruction }) {
        const record = this.props.records.find(record => record.instruction_id === data.item.id);

        return <RecordListItem instruction={data.item}
                               record={record}
                               onEditButtonPress={this.props.onEditButtonPress}
                               onViewButtonPress={this.props.onViewButtonPress}/>;
    }

    render() {
        const { instructions, records } = this.props;

        return (
            <FlatList keyExtractor={this._keyExtractor}
                      data={instructions}
                      extraData={records}
                      renderItem={this.renderItem.bind(this)}/>
        );
    }
}
