// @flow
import type { Template } from '@nativestudios/creace-core/typings/Template';
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { TemplateListItem } from './TemplateListItem';

type Props = {
    templates: Array<Template>;
    onTemplatePress: Function;
};

export class TemplateList extends Component<Props> {
    _keyExtractor(template: Template) {
        return template.id.toString();
    }

    renderItem(data: { item: Template }) {
        return <TemplateListItem template={data.item}
                                 onPress={this.props.onTemplatePress}/>;
    }

    render() {
        const { templates } = this.props;

        templates.sort((a, b) => b.priority - a.priority);

        return (
            <FlatList keyExtractor={this._keyExtractor}
                      data={templates}
                      renderItem={this.renderItem.bind(this)}/>
        );
    }
}
