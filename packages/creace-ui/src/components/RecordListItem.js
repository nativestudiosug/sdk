// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import React from 'react';
import { Text, View } from 'react-native';
import { Button } from './Button';

type Props = {
    instruction: Instruction;
    onEditButtonPress: Function;
    onViewButtonPress: Function;
    record?: Record;
};

export class RecordListItem extends ContextualComponent<Props> {
    render() {
        const { instruction } = this.props;
        const { contentContainer, container, text, buttonContainer } = this.theme.recordListItemTheme;

        return (
            <View style={[container]}>
                <View style={[contentContainer]}>
                    <Text style={[text]}>
                        {instruction.position}: {instruction.instruction_text}
                    </Text>
                </View>
                <View style={[buttonContainer]}>
                    {this.renderButtons()}
                </View>
            </View>
        );
    }

    renderButtons() {
        const { record, instruction } = this.props;
        const { type } = instruction;
        const { button } = this.theme.recordListItemTheme;
        const buttons = [];

        if (record !== undefined && type !== 'text') {
            buttons.push(
                <Button key="view"
                        style={[button]}
                        text={this.translate(`record.list.item.${type}.button.view`)}
                        onPress={() => this.props.onViewButtonPress(instruction)}/>
            );
        }

        buttons.push(
            <Button key="button"
                    style={[button]}
                    text={record === undefined ? this.translate(`record.list.item.${type}.button.add`) : this.translate(`record.list.item.${type}.button.edit`)}
                    onPress={() => this.props.onEditButtonPress(instruction)}/>,
        );

        return buttons;
    }
}
