// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Category } from '@nativestudios/creace-core/typings/Category';
import React from 'react';
import { ImageBackground, Text, TouchableHighlight, View } from 'react-native';
import imageCacheHoc from 'react-native-image-cache-hoc';

type Props = {
    category: Category;
    onPress: Function;
};

type State = {
    height: number;
};

export class CategoryListItem extends ContextualComponent<Props, State> {
    state: State = {
        height: 0,
    };

    render() {
        return (
            <View onLayout={this.onLayout.bind(this)}>
                {this.renderContent()}
            </View>
        );
    }

    renderContent() {
        const { height } = this.state;
        if (height === 0) {
            return <View/>;
        }

        const { category } = this.props;
        const previewImage = category.preview_image || undefined;
        const CachableImage = imageCacheHoc(ImageBackground);
        const { image } = this.theme.categoryListItemTheme;

        return (
            <TouchableHighlight onPress={() => this.props.onPress(category)}>
                <CachableImage style={[image, { height }]}
                               source={{ uri: previewImage }}
                               resizeMode="cover">
                    {this.renderText()}
                </CachableImage>
            </TouchableHighlight>
        );
    }

    renderText() {
        const { category } = this.props;

        if (category.text_position === 'hidden') {
            return <View/>;
        }

        const { text } = this.theme.categoryListItemTheme;
        const textAlign: any = category.text_position;

        return (
            <Text style={[text, { textAlign }]}>
                {category.name}
            </Text>
        );
    }

    onLayout(event: any) {
        const { width } = event.nativeEvent.layout;
        const { category } = this.props;
        const height = width / 100 * (category.height || 50);

        this.setState({ height });
    }
}
