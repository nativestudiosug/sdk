// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import React from 'react';
import { ImageBackground, Text, View } from 'react-native';
import imageCacheHoc from 'react-native-image-cache-hoc';
import { Button } from './Button';

type Props = {
    video: Video;
    template: Template;
    records: Array<Record>;
    onEditButtonPress: Function;
    onUploadButtonPress: Function;
    onDeleteButtonPress: Function;
};

export class VideoListItemUnfinished extends ContextualComponent<Props> {
    render() {
        const { video, template } = this.props;
        const { createdAt, templateName, container, rightContainer, leftContainer, button } = this.theme.videoListItemTheme;
        const CachableImageBackground = imageCacheHoc(ImageBackground);

        return (
            <CachableImageBackground style={[container]} source={{ uri: template.preview_image }} resizeMode="cover">
                <View style={[leftContainer]}>
                    <Text style={[createdAt]}>{video.created_at}</Text>
                    <Text style={[templateName]}>{video.template.name}</Text>
                </View>
                <View style={[rightContainer]}>
                    <Button style={[button]}
                            onPress={() => this.props.onEditButtonPress(video)}
                            text={this.translate('video.list.item.button.edit')}/>
                    {this.renderUploadButton()}
                    <Button style={[button]}
                            onPress={() => this.props.onDeleteButtonPress(video)}
                            text={this.translate('video.list.item.button.delete')}/>
                </View>
            </CachableImageBackground>
        );
    }

    renderUploadButton() {
        const { video, records, template } = this.props;

        for (const instruction of template.instructions) {
            if (!records.find((r: Record) => r.video_id === video.id && r.instruction_id === instruction.id)) {
                return <View/>;
            }
        }

        const { button } = this.theme.videoListItemTheme;
        return (
            <Button style={[button]}
                    onPress={() => this.props.onUploadButtonPress(video)}
                    text={this.translate('video.list.item.button.upload')}/>
        );
    }
}
