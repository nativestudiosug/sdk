// @flow
import { faCog, faPause, faPlay, faShareAlt, faTimes } from '@fortawesome/free-solid-svg-icons';
import { ContextualComponent, File, stringReplace, VideoApi } from '@nativestudios/creace-core';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import CameraRoll from '@react-native-community/cameraroll';
import React from 'react';
import { Alert, Dimensions, Linking, PermissionsAndroid, Text, View } from 'react-native';
import Share from 'react-native-share';
import { IconButton } from './IconButton';
import { OptionsPicker } from './OptionsPicker';
import { VideoPlayer } from './VideoPlayer';

type Props = {
    video: Video;
    template: Template;
    file: File;
    onError?: Function;
    onEnd?: Function;
    onEdit?: Function;
    onDelete?: Function;
    onDuplicate?: Function;
    onQuit?: Function;
};

type State = {
    paused: boolean;
};

export class VideoView extends ContextualComponent<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            paused: true,
        };
    }

    render() {
        return (
            <View style={this.theme.pageTheme}>
                <VideoPlayer file={this.props.file}
                             poster={this.props.video.preview_image}
                             controls={this.renderControls.bind(this)}
                             paused={this.state.paused}
                             onError={this.props.onError}
                             onEnd={this.onEnd.bind(this)}/>
            </View>
        );
    }

    renderControls({ paused, currentTime, duration }) {
        const { absoluteContainer, bar, durationText, controls, barCurrentTime, currentTimeText } = this.theme.videoPlayerTheme;
        const maxWidth = Dimensions.get('screen').width - 20;
        const width = duration > 0 ? Math.min(maxWidth, maxWidth * currentTime / duration) : 0;

        return (
            <View style={[absoluteContainer, controls]}>
                <View style={this.theme.buttonsTopRightTheme}>
                    <IconButton onPress={this.props.onQuit} icon={faTimes} style={{ backgroundColor: 'transparent' }}/>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    {this.renderPlayButton(paused)}
                    {this.renderSettingsButton()}
                    {this.renderShareButton()}
                </View>
                <Text style={[currentTimeText]}>{currentTime}</Text>
                <Text style={[durationText]}>{duration}</Text>
                <View style={[bar]}/>
                <View style={[barCurrentTime, { width }]}/>
            </View>
        );
    }

    renderPlayButton(paused: boolean) {
        const icon = paused ? faPlay : faPause;

        return <IconButton style={{ marginLeft: 20, marginRight: 20 }}
                           onPress={this.onPlayButtonPress.bind(this)}
                           icon={icon}/>;
    }

    renderSettingsButton() {
        const data = [
            {
                key: 'edit',
                label: this.translate('video.view.action.edit'),
            }, {
                key: 'delete',
                label: this.translate('video.view.action.delete'),
            },
        ];

        return (
            <OptionsPicker data={data}
                           onChange={this.handleAction.bind(this)}>
                <IconButton style={{ marginLeft: 20, marginRight: 20 }}
                            icon={faCog}/>
            </OptionsPicker>
        );
    }

    renderShareButton() {
        const { template } = this.props;
        const data = [];
        for (const type of ['share_with_friends', 'camera_roll', 'extra', 'duplicate_template']) {
            if (typeof template.sharing[type] === 'undefined') {
                continue;
            }

            data.push({
                key: type,
                label: template.sharing[type].button_text,
            });
        }

        if (!data.length) {
            return <View/>;
        }

        return (
            <OptionsPicker data={data} onChange={this.handleAction.bind(this)}>
                <IconButton style={{ marginLeft: 20, marginRight: 20 }}
                            icon={faShareAlt}/>
            </OptionsPicker>
        );
    }

    async handleAction({ key }) {
        switch (key) {
            case 'edit':
                this.props.onEdit(this.props.video);
                break;
            case 'delete':
                this.props.onDelete(this.props.video);
                break;
            case 'duplicate':
            case 'duplicate_template':
                this.props.onDuplicate(this.props.video);
                break;
            case 'share_with_friends':
                this.nativeShare();
                break;
            case 'extra':
                this.extraShare();
                break;
            case 'camera_roll':
                this.save();
                break;
        }
    }

    onPlayButtonPress() {
        this.setState({
            paused: !this.state.paused,
        });
    }

    async nativeShare() {
        const { video, template, file } = this.props;

        this.analytics.trackEvent('share_with_friends', {
            video_id: video.id,
        });

        let message = template.sharing_text + ' ' + VideoApi.getVideoUrl(video);

        try {
            await Share.open({
                message,
                url: file.path,
                mime: file.mimeType,
            });
        } catch (e) {
            if (e && e.message && e.message === 'User did not share') {
                // ignore this fucking shit
                return;
            }

            this.errorHandler.notifyError(e);
        }
    }

    async extraShare() {
        const { video, template, device } = this.props;

        let { visit_url, share_url } = template.sharing.extra;

        this.analytics.trackEvent('share_extra', {
            video_id: video.id,
            visit_url: visit_url || '',
            share_url: share_url || '',
        });

        if (share_url) {
            try {
                await fetch(stringReplace(share_url, { video }));
            } catch (error) {
                this.errorHandler.notifyError(error);
            }

            if (!visit_url) {
                Alert.alert(this.translate('video.view.sharing.extra.title'));
            }
        }

        if (visit_url) {
            visit_url = visit_url.replace('{language}', device.locale.toLowerCase());
            visit_url = visit_url.replace('{video.id}', video.id);
            visit_url = visit_url.replace('{video.uid}', video.uid);
            visit_url = visit_url.replace('{device.token}', device.token);
            try {
                await Linking.openURL(visit_url);
            } catch (e) {
                this.errorHandler.notifyError(error);
            }
        }
    }

    async save() {
        const { video, file } = this.props;

        this.analytics.trackEvent('save_to_camera_roll', {
            video_id: video.id,
        });

        try {
            if (Platform.OS === 'android') {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: this.translate('video.view.save.permissionRequest.title'),
                        message: this.translate('video.view.save.permissionRequest.message'),
                    }
                );

                if (!granted) {
                    return;
                }
            }

            await CameraRoll.save(file.path);
            Alert.alert(this.translate('video.view.save.done.title'));
        } catch (error) {
            Alert.alert(this.translate('video.view.save.error.title'));
            this.errorHandler.notifyError(error);
        }
    }

    onEnd() {
        this.setState({
            paused: true,
        });

        this.props.onEnd && this.props.onEnd();
    }
}
