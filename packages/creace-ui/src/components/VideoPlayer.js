// @flow
import { faPause, faPlay } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { ContextualComponent, File } from '@nativestudios/creace-core';
import React, { Element } from 'react';
import { ActivityIndicator, Dimensions, Text, TouchableHighlight, TouchableWithoutFeedback, View } from 'react-native';
import Video from 'react-native-video';

type Props = {
    file?: File;
    uri?: string;
    paused?: boolean;
    poster?: string;
    controls?: () => Element;
    onEnd?: Function;
    onError?: Function;
};

type State = {
    uri: string;
    paused: boolean;
    isLoading: boolean;
    showControls: boolean;
    currentTime: number;
    duration: number;
};

export class VideoPlayer extends ContextualComponent<Props, State> {
    static getDerivedStateFromProps(props: Props, state: State): State {
        const derivedState: State = {
            ...state,
        };

        if (props.file) {
            derivedState.uri = props.file.path;
        } else if (props.uri) {
            derivedState.uri = props.uri;
        }

        if (typeof props.paused === 'undefined') {
            return derivedState;
        }

        derivedState.paused = props.paused;
        if (!props.paused && state.paused) {
            derivedState.showControls = false;
        }

        return derivedState;
    }

    showControlsTimeout: number;

    player: ?Video;

    constructor(props: Props) {
        super(props);

        this.state = {
            uri: '',
            isLoading: false,
            showControls: false,
            currentTime: 0,
            duration: 0,
        };
    }

    render() {
        const { container, videoContainer } = this.theme.videoPlayerTheme;

        return (
            <View style={container}>
                <TouchableWithoutFeedback onPress={this.showControls.bind(this)}>
                    <View style={videoContainer}>
                        {this.renderVideo()}
                        {this.renderLoadingIndicator()}
                        {this.renderControls()}
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    renderVideo() {
        if (!this.state.uri || !this.state.uri.length) {
            return <View/>;
        }

        const { video } = this.theme.videoPlayerTheme;

        return (
            <Video ref={(player) => this.player = player}
                   resizeMode="contain"
                   poster={this.props.poster || undefined}
                   source={{ uri: this.state.uri }}
                   paused={this.state.paused}
                   style={video}
                   onLoadStart={this.onLoadStart.bind(this)}
                   onLoad={this.onLoad.bind(this)}
                   onProgress={this.onProgress.bind(this)}
                   onEnd={this.onEnd.bind(this)}
                   onError={this.props.onError}/>
        );
    }

    renderLoadingIndicator() {
        if (!this.state.isLoading) {
            return <View/>;
        }

        const { absoluteContainer } = this.theme.videoPlayerTheme;
        return (
            <View style={[absoluteContainer]}>
                <ActivityIndicator size="large" color={this.theme.colors.primary}/>
            </View>
        );
    }

    renderControls() {
        let { paused, currentTime, isLoading, duration, showControls } = this.state;

        if (isLoading || (!showControls && !paused)) {
            return <View/>;
        }

        currentTime = this.getHumanizedTime(currentTime);
        duration = this.getHumanizedTime(duration);

        if (this.props.controls) {
            return this.props.controls({ paused, currentTime, duration });
        }

        const playIcon = paused ? faPlay : faPause;
        const maxWidth = Dimensions.get('screen').width - 20;
        const width = duration > 0 ? Math.min(maxWidth, maxWidth * currentTime / duration) : 0;
        const { absoluteContainer, bar, durationText, controls, barCurrentTime, togglePlay, currentTimeText } = this.theme.videoPlayerTheme;

        return (
            <View style={[absoluteContainer, controls]}>
                <TouchableHighlight style={[togglePlay]} onPress={this.toggle.bind(this)}>
                    <FontAwesomeIcon icon={playIcon} color="white" size={30}/>
                </TouchableHighlight>
                <Text style={[currentTimeText]}>{currentTime}</Text>
                <Text style={[durationText]}>{duration}</Text>
                <View style={[bar]}/>
                <View style={[barCurrentTime, { width }]}/>
            </View>
        );
    }

    showControls() {
        window.clearTimeout(this.showControlsTimeout);

        this.setState({
            showControls: true,
        });

        this.showControlsTimeout = window.setTimeout(() => {
            this.setState({
                showControls: false,
            });
        }, __DEV__ ? 60000 : 5000);
    }

    getHumanizedTime(seconds: number) {
        seconds = Math.round(seconds);
        let minutes = Math.floor(seconds / 60);
        seconds -= minutes * 60;

        return [
            (minutes < 10 ? '0' : '') + minutes,
            (seconds < 10 ? '0' : '') + seconds,
        ].join(':');
    }

    toggle() {
        const { paused } = this.state;

        this.setState({ paused: !paused });
    }

    onLoadStart() {
        this.setState({ isLoading: true });
    }

    onLoad({ currentTime, duration }) {
        this.setState({ isLoading: false, currentTime, duration });
    }

    onProgress({ currentTime }) {
        this.setState({ currentTime });
    }

    onEnd() {
        this.setState({
            paused: true,
        });

        this.props.onEnd && this.props.onEnd();

        window.setTimeout(() => {
            this.player && this.player.seek(0);
        });
    }
}
