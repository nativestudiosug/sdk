// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { ActivityIndicator, Animated, Dimensions, Text, View } from 'react-native';
import Video from 'react-native-video';

type Props = {
    overallProgress: number;
    backgroundVideo: string;
    progressText: string;
};

type State = {
    width: Animated.Value;
};

export class VideoUploader extends ContextualComponent<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            width: new Animated.Value(0),
        };
    }

    render() {
        const { progressContainer, container, progressText: progressTextStyle, progressBar } = this.theme.videoUploaderTheme;
        const { overallProgress, progressText } = this.props;
        const { width } = Dimensions.get('screen');

        Animated.timing(this.state.width, {
            toValue: width * overallProgress,
            duration: 300,
        }).start();

        return (
            <View style={[container]}>
                {this.renderBackgroundVideo()}
                <ActivityIndicator size="large" color={this.theme.colors.primary}/>
                <View style={[progressContainer]}>
                    <Animated.View style={[progressBar, { width: this.state.width }]} />
                    <Text style={[progressTextStyle]}>{progressText}</Text>
                </View>
            </View>
        );
    }

    renderBackgroundVideo() {
        const { backgroundVideo } = this.props;

        if (!backgroundVideo) {
            return <View/>;
        }

        return <Video source={{ uri: backgroundVideo }}
                      resizeMode="contain"
                      repeat={true}
                      paused={false}
                      style={[this.theme.videoUploaderTheme.backgroundVideo]}/>
    }
}
