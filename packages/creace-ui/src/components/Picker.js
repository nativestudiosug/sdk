// @flow
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { ActionSheetIOS, Picker as NativePicker, Platform, Text, TouchableHighlight, View } from 'react-native';

type Props = {
    data: {
        [key: string]: string;
    };
    value: string;
    onValueChange: Function;
};

export class Picker extends ContextualComponent<Props> {
    showActionSheet() {
        const { data } = this.props;
        const keys = Object.keys(data);
        const options = keys.map(key => data[key]);
        ActionSheetIOS.showActionSheetWithOptions({ options }, (index) => {
            this.props.onValueChange(keys[index]);
        });
    }

    render() {
        return Platform.select({
            ios: this.renderIOSPicker(),
            android: this.renderAndroidPicker(),
        });
    }

    renderIOSPicker() {
        const { value, data } = this.props;

        return (
            <TouchableHighlight onPress={this.showActionSheet.bind(this)}
                                style={{ height: 40, marginBottom: 20 }}>
                <View style={{
                    position: 'relative',
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderBottomWidth: 1,
                    borderColor: this.theme.colors.light,
                }}>
                    <Text style={[this.theme.textStyles.common]}>
                        {data[value]}
                    </Text>
                    <View style={{ position: 'absolute', top: 0, right: 10, bottom: 0, justifyContent: 'center' }}>
                        <FontAwesomeIcon icon={faAngleDown} color="white"/>
                    </View>
                </View>
            </TouchableHighlight>
        );
    }

    renderAndroidPicker() {
        const { data } = this.props;
        const items = [];

        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }

            items.push(
                <NativePicker.Item key={key} label={data[key]} value={key}/>,
            );
        }

        return (
            <View style={{ borderBottomWidth: 1, borderBottomColor: this.theme.colors.light }}>
                <NativePicker
                    style={{ color: this.theme.colors.light }}
                    onValueChange={this.props.onValueChange}
                    selectedValue={this.props.value}>
                    {items}
                </NativePicker>
            </View>
        );
    }
}
