// @flow
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { StyleProp, TouchableHighlight, ViewStyle } from 'react-native';

type Props = {
    icon: IconDefinition;
    onPress?: Function;
    size?: number;
    style?: StyleProp<ViewStyle>;
};

export class IconButton extends ContextualComponent<Props> {
    static defaultProps: Props = {
        size: 30,
        style: {},
    };

    render() {
        return (
            <TouchableHighlight style={[this.theme.iconButtonTheme, this.props.style]}
                                onPress={this.props.onPress}>
                <FontAwesomeIcon icon={this.props.icon} color="white" size={this.props.size}/>
            </TouchableHighlight>
        );
    }
}
