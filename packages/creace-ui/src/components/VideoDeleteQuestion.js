// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { Switch, Text, View } from 'react-native';
import { Button } from './Button';

type Props = {
    enableSwitches: boolean;
    onAccept: Function;
    onAbort: Function;
}

type State = {
    deleteLocal: boolean;
    deleteRemote: boolean;
}

export class VideoDeleteQuestion extends ContextualComponent<Props, State> {
    static defaultProps: Props = {
        enableSwitches: true,
    };

    state: State = {
        deleteLocal: false,
        deleteRemote: false,
    };

    static getDerivedStateFromProps(props: Props, state: State) {
        if (props.enableSwitches) {
            return state;
        }

        state.deleteRemote = true;
        state.deleteLocal = true;

        return state;
    }

    render() {
        const { onAccept, onAbort } = this.props;
        const { container, text, button, buttonsContainer } = this.theme.videoDeleteQuestonTheme;
        const { deleteLocal, deleteRemote } = this.state;

        return (
            <View style={[container]}>
                <Text style={text}>{this.translate('video.delete.question.text')}</Text>
                {this.renderLocalSwitch()}
                {this.renderRemoteSwitch()}
                <View style={[buttonsContainer]}>
                    <Button style={[button]}
                            disabled={deleteLocal === false && deleteRemote === false}
                            text={this.translate('video.delete.question.button.accept')}
                            onPress={() => onAccept(this.state.deleteLocal, this.state.deleteRemote)}/>
                    <Button style={[button]}
                            text={this.translate('video.delete.question.button.abort')}
                            onPress={onAbort}/>
                </View>
            </View>
        );
    }

    renderLocalSwitch() {
        if (!this.props.enableSwitches) {
            return <View />;
        }

        const { text, switchContainer } = this.theme.videoDeleteQuestonTheme;
        return (
            <View style={switchContainer}>
                <Switch value={this.state.deleteLocal}
                        onValueChange={(deleteLocal) => this.setState({ deleteLocal })}/>
                <Text style={text}>{this.translate('video.delete.question.switchLocal')}</Text>
            </View>
        );
    }

    renderRemoteSwitch() {
        if (!this.props.enableSwitches) {
            return <View />;
        }

        const { text, switchContainer } = this.theme.videoDeleteQuestonTheme;
        return (
            <View style={switchContainer}>
                <Switch value={this.state.deleteRemote}
                        onValueChange={(deleteRemote) => this.setState({ deleteRemote })}/>
                <Text style={text}>{this.translate('video.delete.question.switchRemote')}</Text>
            </View>
        );
    }
}
