// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import React from 'react';
import { ImageBackground, ScrollView, View } from 'react-native';
import imageCacheHoc from 'react-native-image-cache-hoc';
import Markdown from 'react-native-markdown-renderer';
import ModalSelector from 'react-native-modal-selector';
import { Button } from './Button';

type Props = {
    template: Template;
    videos?: Array<Video>;
    onTemplatePress: Function;
    onVideoPress: Function;
    showProjectVideos?: boolean;
    buttonsDisabled?: boolean;
};

export class TemplateDetail extends ContextualComponent<Props> {
    static defaultProps: Props = {
        showProjectVideos: false,
        buttonsDisabled: false,
    };

    render() {
        const { template } = this.props;
        const CachableImage = imageCacheHoc(ImageBackground);
        const { container, buttons, image, content, button } = this.theme.templateDetailTheme;

        return (
            <View style={[container]}>
                <CachableImage resizeMode="cover"
                               source={{ uri: template.preview_image }}
                               style={[image]}>
                    <ScrollView style={[content]}>
                        <Markdown style={this.theme.markdownTheme}>{template.preview_description}</Markdown>
                    </ScrollView>
                    <View style={[buttons]}>
                        <Button style={[button]}
                                disabled={this.props.buttonsDisabled}
                                text={template.preview_cta || this.translate('template.detail.button.start')}
                                onPress={this.props.onTemplatePress}/>
                        {this.renderVideosButton()}
                    </View>
                </CachableImage>
            </View>
        );
    }

    renderVideosButton() {
        const { videos } = this.props;
        const { button } = this.theme.templateDetailTheme;

        if (!videos.length || !this.props.showProjectVideos) {
            return <View/>;
        }

        if (videos.length === 1) {
            return <Button style={[button]}
                           disabled={this.props.buttonsDisabled}
                           text={this.translate('template.detail.button.continue')}
                           onPress={() => this.props.onVideoPress(videos[0])}/>;
        }

        const data = videos.map((video: Video) => {
            return {
                key: video.id,
                label: `${video.created_at}`,
                video,
            };
        });

        return (
            <ModalSelector data={data} onChange={(option) => this.props.onVideoPress(option.video)}>
                <Button style={[button]}
                        text={this.translate('template.detail.button.continue')}
                        onPress={() => {
                        }}/>
            </ModalSelector>
        );
    }
}
