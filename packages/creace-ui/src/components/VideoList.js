// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import React, { Element } from 'react';
import { FlatList, Text } from 'react-native';
import { VideoListItem } from './VideoListItem';

type Props = {
    videos: Array<Video>;
    templates: Array<Template>;
    records: Array<Record>;
    onViewButtonPress: Function;
    onEditButtonPress: Function;
    onUploadButtonPress: Function;
    onDeleteButtonPress: Function;
    emptyText?: string;
    emptyComponent?: Element;
};

export class VideoList extends ContextualComponent<Props> {
    _keyExtractor(item: Video) {
        return item.id.toString();
    }

    renderItem(data: { item: Video }) {
        const template = this.props.templates.find(t => t.id === data.item.template.id);
        const records = this.props.records.filter((r: Record) => r.video_id === data.item.id);
        return <VideoListItem video={data.item}
                              template={template}
                              records={records}
                              onViewButtonPress={this.props.onViewButtonPress}
                              onEditButtonPress={this.props.onEditButtonPress}
                              onUploadButtonPress={this.props.onUploadButtonPress}
                              onDeleteButtonPress={this.props.onDeleteButtonPress}/>;
    }

    render() {
        const { videos } = this.props;

        if (!videos.length) {
            return this.props.emptyComponent || (
                <Text style={[this.theme.textStyles.headline, { textAlign: 'center', padding: 20 }]}>
                    {this.props.emptyText || this.translate('video.list.empty')}
                </Text>
            );
        }

        return (
            <FlatList keyExtractor={this._keyExtractor}
                      data={videos}
                      renderItem={this.renderItem.bind(this)}/>
        );
    }
}
