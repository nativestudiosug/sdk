// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import ModalSelector from 'react-native-modal-selector';

type Props = {
    data: Array<any>;
    onChange: Function;
};

export class OptionsPicker extends ContextualComponent<Props> {
    render() {
        return (
            <ModalSelector data={this.props.data}
                           overlayStyle={{ flexDirection: 'column', alignItems: 'center' }}
                           optionContainerStyle={{ backgroundColor: 'transparent' }}
                           optionStyle={[this.theme.buttonTheme.container, { marginTop: 8, borderBottomWidth: 0 }]}
                           optionTextStyle={this.theme.buttonTheme.text}
                           cancelContainerStyle={{ alignSelf: 'center', padding: 10 }}
                           cancelStyle={[this.theme.buttonTheme.container, { borderBottomWidth: 0 }]}
                           cancelTextStyle={this.theme.buttonTheme.text}
                           cancelText={this.translate('modal.cancel.text')}
                           onChange={this.props.onChange}>
                {this.props.children}
            </ModalSelector>
        );
    }
}
