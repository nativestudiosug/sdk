// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { Animated, Easing, Text, View } from 'react-native';

type Props = {
    countdown: number;
    onDone: Function;
};

type State = {
    opacities: Array<Animated.Value>;
};

export class Countdown extends ContextualComponent<Props, State> {
    constructor(props: Props) {
        super(props);

        const opacities: Array<Animated.Value> = [];
        let { countdown } = this.props;
        let index = 0;
        while (countdown) {
            opacities[index] = new Animated.Value(0);
            countdown--;
            index++;
        }

        this.state = {
            opacities,
        };
    }

    showNumber(index: number) {
        if (this.state.opacities[index]) {
            Animated
                .timing(this.state.opacities[index], {
                    duration: 500,
                    toValue: 1,
                    easing: Easing.linear,
                })
                .start(() => {
                    this.hideNumber(index);
                });
        } else {
            this.props.onDone();
        }
    }

    hideNumber(index: number) {
        Animated
            .timing(this.state.opacities[index], {
                duration: 500,
                toValue: 0,
                easing: Easing.linear,
            })
            .start(() => {
                this.showNumber(++index);
            });
    }

    render() {
        const countdowns: Array = [];
        let { countdown } = this.props;
        let index = 0;
        const { container, text } = this.theme.countdownTheme;
        while (countdown) {
            countdowns.push(
                <Animated.View
                    style={{ opacity: this.state.opacities[index], position: index > 0 ? 'absolute' : 'relative' }}
                    key={countdown}>
                    <Text style={[text]}>
                        {countdown}
                    </Text>
                </Animated.View>,
            );
            countdown--;
            index++;
        }

        this.showNumber(0);

        return (
            <View style={[container]}>
                {countdowns}
            </View>
        );
    }
}
