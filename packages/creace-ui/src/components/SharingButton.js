// @flow
import { faShareAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { TouchableHighlight } from 'react-native';

type Props = {
    onPress: Function;
};

export class SharingButton extends ContextualComponent<Props> {
    render() {
        return (
            <TouchableHighlight style={[this.theme.iconButtonTheme]}
                                onPress={this.props.onPress}>
                <FontAwesomeIcon icon={faShareAlt} color="white" size={30}/>
            </TouchableHighlight>
        );
    }
}
