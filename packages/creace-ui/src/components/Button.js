// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { StyleProp, Text, TouchableHighlight, View, ViewStyle } from 'react-native';

type Props = {
    onPress: Function;
    text: string;
    disabled?: boolean;
    fontSize?: number;
    style?: StyleProp<ViewStyle>;
    width?: number;
}

export class Button extends ContextualComponent<Props> {
    static defaultProps: Props = {
        fontSize: 17,
        disabled: false,
        style: {},
    };

    render() {
        let { style, onPress, disabled } = this.props;

        if (disabled) {
            return (
                <View style={style}>
                    {this.renderContent()}
                </View>
            );
        }

        return (
            <TouchableHighlight onPress={onPress} style={style} underlayColor="transparent">
                {this.renderContent()}
            </TouchableHighlight>
        );
    }

    renderContent() {
        const { theme } = this;
        const { text, width, fontSize, disabled } = this.props;
        const opacity = disabled ? 0.5 : 1;

        return (
            <View style={[theme.buttonTheme.container, { width, opacity }]}>
                <Text
                    style={[theme.buttonTheme.text, { fontSize }]}>
                    {text}
                </Text>
            </View>
        );
    }
}

