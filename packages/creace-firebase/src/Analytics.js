import { Analytics as CoreAnalytics } from '@nativestudios/creace-core';
import { analytics } from 'react-native-firebase';

export class Analytics extends CoreAnalytics {
    trackScreen(screenName: string): void {
        analytics().setCurrentScreen(screenName);
    }

    trackEvent(name: string, data?: any): void {
        analytics().logEvent(name, data);
    }
}
