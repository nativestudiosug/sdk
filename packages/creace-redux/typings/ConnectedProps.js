import type { Advertising } from '@nativestudios/creace-core/typings/Advertising';
import type { Category } from '@nativestudios/creace-core/typings/Category';
import type { Device } from '@nativestudios/creace-core/typings/Device';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import * as actions from '../lib/actions';

export type ConnectedProps = {
    device: Device;
    categories: Array<Category>;
    templates: Array<Template>;
    videos: Array<Video>;
    records: Array<Record>;
    advertisings: Array<Advertising>;
    settings: {
        [key: string]: string;
    };
    updateDevice: actions.updateDevice;
    setCategories: actions.setCategories;
    setTemplates: actions.setTemplates;
    addTemplates: actions.addTemplates;
    addOrUpdateVideo: actions.addOrUpdateVideo;
    deleteVideo: actions.deleteVideo;
    addOrUpdateRecord: actions.addOrUpdateRecord;
    setAdvertisings: actions.setAdvertisings;
    setSetting: actions.setSetting;
};
