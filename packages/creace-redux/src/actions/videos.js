// @flow
import type { Video } from '@nativestudios/creace-core/typings/Video';

export const VIDEO_ADD_OR_UPDATE = 'VIDEO_ADD_OR_UPDATE';
export const VIDEO_DELETE = 'VIDEO_DELETE';

export function addOrUpdateVideo(video: Video) {
    return {
        type: VIDEO_ADD_OR_UPDATE,
        data: video,
    };
}

export function deleteVideo(video: Video) {
    return {
        type: VIDEO_DELETE,
        data: video,
    };
}
