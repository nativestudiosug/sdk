// @flow
import type { Category } from '@nativestudios/creace-core/typings/Category';

export const CATEGORIES_SET = 'CATEGORIES_SET';

export function setCategories(categories: Array<Category>) {
    return {
        type: CATEGORIES_SET,
        data: categories,
    };
}
