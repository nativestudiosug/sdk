// @flow
export const SETTINGS_SET = 'SETTINGS_SET';

export function setSetting(name: string, value: string) {
    return {
        type: SETTINGS_SET,
        name,
        value,
    }
}
