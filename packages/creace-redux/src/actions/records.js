// @flow
import type { Record } from '@nativestudios/creace-core/typings/Record';

export const RECORD_ADD_OR_UPDATE = 'RECORD_ADD_OR_UPDATE';

export function addOrUpdateRecord(record: Record) {
    return {
        type: RECORD_ADD_OR_UPDATE,
        data: record,
    };
}
