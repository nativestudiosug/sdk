// @flow
import type { Template } from '@nativestudios/creace-core/typings/Template';

export const TEMPLATES_SET = 'TEMPLATES_SET';
export const TEMPLATES_ADD = 'TEMPLATES_ADD';

export function setTemplates(templates: Array<Template>) {
    return {
        type: TEMPLATES_SET,
        data: templates,
    };
}

export function addTemplates(templates: Array<Template>) {
    return {
        type: TEMPLATES_ADD,
        data: templates,
    };
}
