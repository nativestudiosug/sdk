// @flow
import type { Device } from '@nativestudios/creace-core/typings/Device';

export const DEVICE_UPDATE = 'DEVICE_UPDATE';

export function updateDevice(device: Device) {
    return {
        type: DEVICE_UPDATE,
        data: device,
    };
}
