// @flow
import type { Advertising } from '@nativestudios/creace-core/typings/Advertising';

export const ADVERTISING_SET = 'ADVERTISING_SET';

export function setAdvertisings(advertisings: Array<Advertising>) {
    return {
        type: ADVERTISING_SET,
        data: advertisings,
    };
}

