// @flow
import { ContextualComponent, VideoApi } from '@nativestudios/creace-core';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import { Button, RecordList, VideoDeleteQuestion } from '@nativestudios/creace-ui';
import React from 'react';
import { View } from 'react-native';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

type Props = {
    video: Video;
    onEditButtonPress: Function;
    onViewButtonPress: Function;
    onUploadButtonPress: Function;
    onDelete: Function;
};

type State = {
    wantsToDelete: boolean;
};

export class _ConnectedRecordList extends ContextualComponent<Props & ConnectedProps, State> {
    state: State = {
        wantsToDelete: false,
    };

    render() {
        let { video, templates, records } = this.props;
        const template = templates.find((template: Template) => template.id === video.template.id);
        records = records.filter((record: Record) => record.video_id === video.id);

        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <RecordList instructions={template.instructions}
                            records={records}
                            onEditButtonPress={this.onEditButtonPress.bind(this)}
                            onViewButtonPress={this.onViewButtonPress.bind(this)}/>
                <View style={{ padding: 10, borderTopWidth: 1, borderTopColor: this.theme.colors.light }}>
                    {this.renderUploadButton()}
                    <Button onPress={this.onDeleteButtonPress.bind(this)}
                            text={this.translate('video.list.item.button.delete')}/>
                </View>
                {this.renderDeleteQuestion()}
            </View>
        );
    }

    renderUploadButton() {
        const { video, records, templates } = this.props;
        const template = templates.find((template: Template) => template.id === video.template.id);

        for (const instruction of template.instructions) {
            if (!records.find((r: Record) => r.video_id === video.id && r.instruction_id === instruction.id)) {
                return <View/>;
            }
        }

        return (
            <Button style={{ marginBottom: 10 }}
                    onPress={() => this.props.onUploadButtonPress(video)}
                    text={this.translate('video.list.item.button.upload')}/>
        );
    }

    renderDeleteQuestion() {
        if (!this.state.wantsToDelete) {
            return <View/>;
        }

        return <VideoDeleteQuestion enableSwitches={false}
                                    onAccept={this.onVideoDeleteAccept.bind(this)}
                                    onAbort={this.onVideoDeleteAbort.bind(this)}/>;
    }

    onEditButtonPress(instruction: Instruction) {
        this.props.onEditButtonPress(this.props.video, instruction);
    }

    onViewButtonPress(instruction: Instruction) {
        this.props.onViewButtonPress(this.props.video, instruction);
    }

    onDeleteButtonPress() {
        this.setState({
            wantsToDelete: true,
        });
    }

    async onVideoDeleteAccept(deleteLocal: boolean, deleteRemote: boolean) {
        if (deleteRemote) {
            await VideoApi.destroy(this.props.video.id);
        }

        if (deleteLocal) {
            this.props.deleteVideo(this.props.video);
        }

        this.setState({
            wantsToDelete: false,
        });

        this.props.onDelete(this.props.video);
    }

    onVideoDeleteAbort() {
        this.setState({
            wantsToDelete: false,
        });
    }
}

export const ConnectedRecordList = creaceConnect(_ConnectedRecordList);
