// @flow
import { CategoryApi, ContextualComponent, DeviceApi, TemplateApi } from '@nativestudios/creace-core';
import { availableTranslations } from '@nativestudios/creace-i18n';
import { Picker } from '@nativestudios/creace-ui';
import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

export class _ConnectedSettings extends ContextualComponent<ConnectedProps> {
    props: ConnectedProps;

    render() {
        return (
            <ScrollView style={[this.theme.pageTheme, { padding: 20 }]}>
                {this.renderLanguageSettings()}
                {this.renderCountdownSetting()}
            </ScrollView>
        );
    }

    renderLanguageSettings() {
        const localeKeys = Object.keys(availableTranslations);
        const locales = {};
        for (const key of localeKeys) {
            locales[key] = this.translate(`locale.${key}`);
        }

        return (
            <View>
                <Text style={this.theme.textStyles.headline}>{this.translate('settings.labels.language')}</Text>
                <Picker data={locales} value={this.props.device.locale} onValueChange={this.onLocaleChange.bind(this)}/>
            </View>
        );
    }

    renderCountdownSetting() {
        const { settings } = this.props;
        const value = settings.countdown || 'enabled';
        const data = {
            enabled: this.translate('settings.values.countdown.enabled'),
            disabled: this.translate('settings.values.countdown.disabled'),
        };
        return (
            <View style={{ marginTop: 20 }}>
                <Text style={this.theme.textStyles.headline}>{this.translate('settings.labels.countdown')}</Text>
                <Picker data={data} value={value} onValueChange={this.onCountdownChange.bind(this)}/>
            </View>
        );
    }

    async onLocaleChange(locale: string) {
        console.log('onLocaleChange', locale);
        let { device, setCategories, updateDevice, setTemplates } = this.props;
        device = await DeviceApi.register({
            ...device,
            locale,
        });
        updateDevice(device);
        this.translator.setLanguage(device.locale);

        const [categories, templates] = await Promise.all([
            CategoryApi.fetch(),
            TemplateApi.fetch(),
        ]);

        setCategories(categories);
        setTemplates(templates);
    }

    onCountdownChange(value) {
        this.props.setSetting('countdown', value);
    }
}

export const ConnectedSettings = creaceConnect(_ConnectedSettings);
