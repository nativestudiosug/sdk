// @flow
import { ContextualComponent, File, VideoApi } from '@nativestudios/creace-core';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import { VideoDeleteQuestion, VideoView } from '@nativestudios/creace-ui';
import React from 'react';
import { Alert, ActivityIndicator, View } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

type Props = {
    video: Video;
    onError?: Function;
    onEnd?: Function;
    onDelete?: Function;
    onEdit?: Function;
    onQuit?: Function;
};

type State = {
    file: File;
    video: Video;
    wantsToDelete: boolean;
};

const noop = () => {
};

export class _ConnectedVideoView extends ContextualComponent<Props & ConnectedProps, State> {
    static defaultProps: Props = {
        onError: noop,
        onEnd: noop,
        onDelete: noop,
        onEdit: noop,
        onQuit: noop,
    };

    state: State = {
        video: null,
        wantsToDelete: false,
    };

    async componentDidMount(): void {
        const { video } = this.props;
        const filename = video.url.split('/').pop();
        const path = `${RNFetchBlob.fs.dirs.DocumentDir}/${filename}`;
        const file = new File(path);

        if (await file.exists()) {
            this.setState({ file });
        } else {
            this.downloadVideo();
        }
    }

    async downloadVideo() {
        const { video } = this.props;
        const filename = video.url.split('/').pop();
        const path = `${RNFetchBlob.fs.dirs.DocumentDir}/${filename}`;
        const file = new File(path);

        await RNFetchBlob
            .config({
                path,
            })
            .fetch('GET', video.url);

        if (await file.exists()) {
            this.setState({ file });
        } else {
            this.props.onError(new Error(this.translate('video.view.error.downloadFailed')));
        }
    }

    render() {
        const { file } = this.state;
        if (!file) {
            return (
                <View style={[this.theme.centeredPageTheme]}>
                    <ActivityIndicator size="large" color={this.theme.colors.primary}/>
                </View>
            );
        }

        const { onEnd, onError, onQuit, video, templates, device } = this.props;
        const template = templates.find(template => template.id === video.template.id);

        return (
            <View style={this.theme.pageTheme}>
                <VideoView video={video}
                           template={template}
                           file={file}
                           device={device}
                           onDelete={this.onDelete.bind(this)}
                           onEdit={this.onEdit.bind(this)}
                           onQuit={onQuit}
                           onError={onError}
                           onDuplicate={this.onDuplicate.bind(this)}
                           onEnd={onEnd}/>
                {this.renderDeleteQuestion()}
            </View>
        );
    }

    renderDeleteQuestion() {
        if (!this.state.wantsToDelete) {
            return <View/>;
        }

        return <VideoDeleteQuestion onAccept={this.onVideoDeleteAccept.bind(this)}
                                    onAbort={this.onVideoDeleteAbort.bind(this)}/>;
    }

    async onVideoDeleteAccept(deleteLocal: boolean, deleteRemote: boolean) {
        const { video } = this.state;

        if (deleteRemote) {
            await VideoApi.destroy(video.id);
        }

        if (deleteLocal) {
            this.props.deleteVideo(video);
            this.props.onDelete(video);
        }

        this.setState({
            wantsToDelete: false,
            video: null,
        });
    }

    onVideoDeleteAbort() {
        this.setState({
            wantsToDelete: false,
            video: null,
        });
    }

    onDelete(video: Video) {
        this.setState({
            wantsToDelete: true,
            video,
        });
    }

    async onEdit(video: Video) {
        const { templates, records, videos, addOrUpdateVideo, addOrUpdateRecord } = this.props;
        const editVideo = videos.find((v: Video) => v.originalVideo === video.id && v.state !== 'finished');
        if (editVideo) {
            return this.props.onEdit(editVideo);
        }

        const template = templates.find((template: Template) => template.id === video.template.id);
        const newVideo: Video = await VideoApi.create(template);
        newVideo.originalVideo = video.id;
        addOrUpdateVideo(newVideo);

        for (const record of records) {
            if (record.video_id !== video.id) {
                continue;
            }

            addOrUpdateRecord({
                ...record,
                video_id: newVideo.id,
                state: 'new',
                file_id: null,
            });
        }

        this.props.onEdit(newVideo);
    }

    async onDuplicate(video: Video) {
        try {
            const { templates, records, addOrUpdateVideo, addOrUpdateRecord } = this.props;
            const template = templates.find((template: Template) => template.id === video.template.id);
            const duplicateTemplateId = template.sharing.duplicate_template.duplicate_template_id;
            const duplicateTemplate = templates.find((template: Template) => template.id === duplicateTemplateId);

            if (!duplicateTemplate) {
                throw Error(`Template with ID "${template.id}" can not be duplicated`);
            }

            this.analytics.trackEvent('duplicate_template', {
                video_id: video.id,
            });

            const newVideo: Video = await VideoApi.create(duplicateTemplate);
            addOrUpdateVideo(newVideo);

            for (const record of records) {
                if (record.video_id !== video.id) {
                    continue;
                }

                const instruction = template.instructions.find((instruction: Instruction) => instruction.id === record.instruction_id);
                const duplicateInstruction = duplicateTemplate.instructions.find((duplicateInstruction: Instruction) => duplicateInstruction.position === instruction.position);

                if (!duplicateInstruction) {
                    continue;
                }

                addOrUpdateRecord({
                    ...record,
                    instruction_id: duplicateInstruction.id,
                    video_id: newVideo.id,
                    state: 'new',
                    file_id: null,
                });
            }

            this.props.onEdit(newVideo);
        } catch (error) {
            this.errorHandler.notifyError(error);
            Alert.alert(this.translate('video.view.error.duplicateFailed'))
        }
    }
}

export const ConnectedVideoView = creaceConnect(_ConnectedVideoView);
