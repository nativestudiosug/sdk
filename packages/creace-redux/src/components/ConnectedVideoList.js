// @flow
import { VideoApi } from '@nativestudios/creace-core';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import { VideoDeleteQuestion, VideoList } from '@nativestudios/creace-ui';
import React, { Component, Element } from 'react';
import { View } from 'react-native';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

type Props = {
    onViewButtonPress: Function;
    onEditButtonPress: Function;
    onUploadButtonPress: Function;
    statesBlacklist?: Array<string>;
    statesWhitelist?: Array<string>;
    emptyText?: string;
    emptyComponent?: Element;
};

type State = {
    video: Video;
    wantsToDelete: boolean;
};

export class _ConnectedVideoList extends Component<Props & ConnectedProps, State> {
    state: State = {
        video: null,
        wantsToDelete: false,
    };

    static defaultProps: Props = {
        statesBlacklist: [],
        statesWhitelist: [],
    };

    render() {
        return (
            <View>
                <VideoList videos={this.getVideos()}
                           templates={this.props.templates}
                           records={this.props.records}
                           onViewButtonPress={this.props.onViewButtonPress}
                           onUploadButtonPress={this.props.onUploadButtonPress}
                           onEditButtonPress={this.props.onEditButtonPress}
                           onDeleteButtonPress={this.onDeleteButtonPress.bind(this)}
                           emptyText={this.props.emptyText}
                           emptyComponent={this.props.emptyComponent}/>
                {this.renderDeleteQuestion()}
            </View>
        );
    }

    renderDeleteQuestion() {
        if (!this.state.wantsToDelete) {
            return <View/>;
        }

        return <VideoDeleteQuestion enableSwitches={false}
                                    onAccept={this.onVideoDeleteAccept.bind(this)}
                                    onAbort={this.onVideoDeleteAbort.bind(this)}/>;
    }

    getVideos(): Array<Video> {
        const { videos, statesWhitelist, statesBlacklist } = this.props;

        if (!statesBlacklist.length && !statesWhitelist.length) {
            return videos;
        }

        if (statesBlacklist.length) {
            return videos.filter(video => !statesBlacklist.includes(video.state));
        }

        return videos.filter(video => statesWhitelist.includes(video.state));
    }

    onDeleteButtonPress(video: Video) {
        this.setState({
            video,
            wantsToDelete: true,
        });
    }

    async onVideoDeleteAccept(deleteLocal: boolean, deleteRemote: boolean) {
        if (deleteRemote) {
            await VideoApi.destroy(this.state.video.id);
        }

        if (deleteLocal) {
            this.props.deleteVideo(this.state.video);
        }

        this.setState({
            video: null,
            wantsToDelete: false,
        });
    }

    onVideoDeleteAbort() {
        this.setState({
            wantsToDelete: false,
            video: null,
        });
    }
}

export const ConnectedVideoList = creaceConnect(_ConnectedVideoList);
