// @flow
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import { RecordView } from '@nativestudios/creace-recorder';
import React, { Component } from 'react';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

type Props = {
    video: Video;
    instruction: Instruction;
    onQuit: Function;
    onEnd?: Function;
    onError?: Function;
};

export class _ConnectedRecordView extends Component<Props & ConnectedProps> {
    render() {
        const { records, instruction, video } = this.props;
        const record = records.find((record: Record) => record.video_id === video.id && record.instruction_id === instruction.id);

        return (
            <RecordView record={record}
                        onQuit={this.props.onQuit}
                        onEnd={this.props.onEnd}
                        onError={this.props.onError}/>
        );
    }
}

export const ConnectedRecordView = creaceConnect(_ConnectedRecordView);
