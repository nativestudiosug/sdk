// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import { Recorder } from '@nativestudios/creace-recorder';
import React from 'react';
import { RNCamera } from 'react-native-camera';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

type Props = {
    video: Video,
    onQuit: Function;
    onButtonProcessPress: Function;
    onButtonRecordsPress: Function;
};

type State = {
    cameraType: ?RNCamera.Constants.Type;
};

export class _ConnectedRecorder extends ContextualComponent<Props & ConnectedProps, State> {
    state: State = {
        cameraType: undefined,
    };

    render() {
        const { templates, video, records, settings } = this.props;
        const template: Template = templates.find(template => template.id === video.template.id);
        let currentInstruction: number = 0;
        for (const instruction of template.instructions) {
            if (!records.find((record) => record.instruction_id === instruction.id && record.video_id === video.id)) {
                break;
            }

            currentInstruction++;
        }

        return <Recorder template={template}
                         currentInstruction={currentInstruction}
                         cameraType={this.state.cameraType}
                         showCountdown={settings.countdown !== 'disabled'}
                         onCameraTypeChanged={this.onCameraTypeChanged.bind(this)}
                         onRecord={this.onRecord.bind(this)}
                         onRedo={this.onRedo.bind(this)}
                         onError={this.onError.bind(this)}
                         onQuit={this.props.onQuit}
                         onButtonProcessPress={this.props.onButtonProcessPress}
                         onButtonRecordsPress={this.props.onButtonRecordsPress}/>;
    }

    onCameraTypeChanged(cameraType: RNCamera.Constants.Type) {
        this.analytics.trackEvent('camera_changed', {
            type: cameraType === RNCamera.Constants.Type.front ? 'front' : 'back',
        });
        this.setState({ cameraType });
    }

    onRecord(instruction: Instruction, value: string) {
        this.analytics.trackEvent('record', {
            video_id: this.props.video.id,
            instruction_id: instruction.id,
        });

        const record: Record = {
            instruction_id: instruction.id,
            video_id: this.props.video.id,
            value: value,
            state: 'new',
            created_at: Date.now(),
        };

        this.props.addOrUpdateRecord(record);
    }

    onRedo(instruction: Instruction) {
        this.analytics.trackEvent('record_redo', {
            video_id: this.props.video.id,
            instruction_id: instruction.id,
        });
    }

    onError(instruction: Instruction) {
        this.analytics.trackEvent('record_error', {
            video_id: this.props.video.id,
            instruction_id: instruction.id,
        });
    }
}

export const ConnectedRecorder = creaceConnect(_ConnectedRecorder);
