// @flow
import { VideoApi } from '@nativestudios/creace-core';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import { TemplateDetail } from '@nativestudios/creace-ui';
import React, { Component } from 'react';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

type Props = {
    template: Template;
    onVideo: Function;
    showProjectVideos?: boolean;
};

export class _ConnectedTemplateDetail extends Component<Props & ConnectedProps> {
    state = {
        videoCreationInProgress: false,
    };

    render() {
        const { template, showProjectVideos } = this.props;
        const videos = this.props.videos.filter((video: Video) => video.template.id === template.id && video.state === 'new');
        return <TemplateDetail template={template}
                               videos={videos}
                               buttonsDisabled={this.state.videoCreationInProgress}
                               showProjectVideos={showProjectVideos}
                               onTemplatePress={this.onTemplatePress.bind(this)}
                               onVideoPress={this.onVideoPress.bind(this)}/>;
    }

    async onTemplatePress() {
        if (this.state.videoCreationInProgress) {
            return;
        }

        this.setState({
            videoCreationInProgress: true,
        });

        const video = await VideoApi.create(this.props.template);

        this.props.addOrUpdateVideo(video);
        this.props.onVideo(video);

        this.setState({
            videoCreationInProgress: false,
        });
    }

    onVideoPress(video: Video) {
        this.props.onVideo(video);
    }
}

export const ConnectedTemplateDetail = creaceConnect(_ConnectedTemplateDetail);
