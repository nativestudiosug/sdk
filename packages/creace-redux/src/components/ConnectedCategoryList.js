// @flow
import { getCategoriesWithTemplates, getTemplatesByCategory } from '@nativestudios/creace-core';
import type { Category } from '@nativestudios/creace-core/typings/Category';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import { CategoryList } from '@nativestudios/creace-ui';
import React, { Component } from 'react';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

type Props = {
    categories: Array<Category>,
    templates: Array<Template>,
    onCategoryPress: Function,
};

export class _ConnectedCategoryList extends Component<Props & ConnectedProps> {
    render() {
        const { categories, templates } = this.props;

        return <CategoryList categories={getCategoriesWithTemplates(categories, templates)}
                             onCategoryPress={this.onCategoryPress.bind(this)}/>;
    }

    onCategoryPress(category: Category) {
        const { templates } = this.props;

        this.props.onCategoryPress(category, getTemplatesByCategory(templates, category));
    }
}

export const ConnectedCategoryList = creaceConnect(_ConnectedCategoryList);
