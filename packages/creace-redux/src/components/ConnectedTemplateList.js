// @flow
import { getTemplatesByCategory } from '@nativestudios/creace-core';
import type { Category } from '@nativestudios/creace-core/typings/Category';
import { TemplateList } from '@nativestudios/creace-ui';
import React, { Component } from 'react';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

type Props = {
    category: Category,
    onTemplatePress: Function,
};

export class _ConnectedTemplateList extends Component<Props & ConnectedProps> {
    render() {
        const { category, templates } = this.props;
        return <TemplateList templates={getTemplatesByCategory(templates, category)}
                             onTemplatePress={this.props.onTemplatePress}/>;
    }

}

export const ConnectedTemplateList = creaceConnect(_ConnectedTemplateList);
