// @flow
import { ContextualComponent, VideoApi } from '@nativestudios/creace-core';
import type { File } from '@nativestudios/creace-core/typings/File';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import { VideoUploader } from '@nativestudios/creace-ui';
import NetInfo, { NetInfoState, NetInfoStateType, NetInfoSubscription } from '@react-native-community/netinfo';
import React from 'react';
import { Alert } from 'react-native';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

type Props = {
    video: Video;
    onFinished: Function;
    onError: Function;
};

type State = {
    overallProgress: number;
    progressText: string;
};

export class _ConnectedVideoUploader extends ContextualComponent<Props & ConnectedProps> {
    uploadStarted = false;
    renderingStarted = false;
    ignoreWifi = false;
    netInfoSubscription: NetInfoSubscription;
    netInfoState: NetInfoState;

    state: State = {
        overallProgress: 0,
        progressText: '',
    };

    async componentDidMount(): void {
        this.netInfoState = await NetInfo.fetch();
        this.netInfoSubscription = NetInfo.addEventListener(this.onNetInfoState.bind(this));
        const video = this.getCurrentVideo();

        if (video.state === 'new') {
            return this.handleVideo();
        }

        const remoteVideo = await VideoApi.load(video.id);

        if (remoteVideo.state === video.state) {
            return this.handleVideo();
        }

        this.props.addOrUpdateVideo(remoteVideo);
    }

    async componentDidUpdate(): void {
        this.handleVideo();
    }

    componentWillUnmount(): void {
        this.netInfoSubscription();
    }

    async handleVideo() {
        let { records, templates } = this.props;
        const video = this.getCurrentVideo();
        const template = templates.find((template: Template) => template.id === video.template.id);
        const recordsByVideo = records.filter((record: Record) => record.video_id === video.id);

        switch (video.state) {
            case 'new':
                if (!this.netInfoState.isConnected) {
                    return this.showNotConnectedAlert();
                }

                if (this.netInfoState.type !== NetInfoStateType.wifi && !this.ignoreWifi) {
                    return this.showNotWifiAlert();
                }

                const recordsToUpload = recordsByVideo.filter((record: Record) => record.state === 'new');

                if (!recordsToUpload.length) {
                    return this.props.addOrUpdateVideo({
                        ...video,
                        state: 'uploaded',
                    });
                }

                if (this.uploadStarted) {
                    return;
                }

                this.uploadStarted = true;
                this.analytics.trackEvent('processing_start', {
                    video_id: video.id,
                    template_id: video.template.id,
                });
                try {
                    await Promise.all(
                        recordsToUpload.map(async (record: Record) => {
                            const instruction: Instruction = template.instructions
                                .find((instruction: Instruction) => instruction.id === record.instruction_id);

                            try {
                                const result: File | boolean | null = await VideoApi
                                    .uploadRecord(video, instruction, record);
                                // .uploadProgress(console.log.bind(console, 'uploadProgress'));
                                if (result !== null) {
                                    this.props.addOrUpdateRecord({
                                        ...record,
                                        state: 'uploaded',
                                        file_id: result === true ? null : result.id,
                                    });
                                }
                            } catch (error) {
                                this.analytics.trackEvent('upload_fail', {
                                    video_id: record.video_id,
                                    instruction_id: instruction.id,
                                });
                                throw error;
                            }
                        }),
                    );
                } catch (error) {
                    this.uploadStarted = false;

                    return this.props.onError(error);
                }

                break;
            case 'uploaded':
                if (this.renderingStarted) {
                    return;
                }
                this.renderingStarted = true;
                this.analytics.trackEvent('processing_render', {
                    video_id: video.id,
                    template_id: video.template.id,
                });
                await VideoApi.render(video);
                this.props.addOrUpdateVideo(await VideoApi.load(video.id));

                break;
            case 'failed':
                if (!this.renderingStarted) {
                    this.props.addOrUpdateVideo({
                        ...video,
                        state: 'new',
                    });
                    return;
                }
                this.analytics.trackEvent('processing_failed', {
                    video_id: video.id,
                    template_id: video.template.id,
                });
                this.props.onError();

                break;
            case 'finished':
                this.analytics.trackEvent('processing_finished', {
                    video_id: video.id,
                    template_id: video.template.id,
                });
                this.props.onFinished(video);

                break;
            default:
                window.setTimeout(async () => {
                    this.props.addOrUpdateVideo(await VideoApi.load(video.id));
                }, 5000);
                break;
        }
    }

    getStep() {
        const video = this.getCurrentVideo();

        switch (true) {
            case video.state === 'new':
            case video.state === 'recorded':
                return 1;
            case video.state === 'uploaded':
                return 2;
            case video.state === 'queued':
                return 3;
            case video.state === 'rendering':
                return 5;
            default:
                return 6;
        }
    }

    getRandomAdvertising() {
        const { advertisings } = this.props;
        const index = Math.floor(Math.random() * advertisings.length);

        return typeof advertisings[index] === 'undefined' ? null : advertisings[index].url;
    }

    render() {
        const { video, templates } = this.props;
        const template = templates.find((template: Template) => template.id === video.template.id);

        return (
            <VideoUploader backgroundVideo={template.video_advertisement || this.getRandomAdvertising()}
                           progressText={this.getProgressText()}
                           overallProgress={this.getOverallProgress()}/>
        );
    }

    onNetInfoState(state: NetInfoState) {
        this.netInfoState = state;
    }

    showNotConnectedAlert() {
        return Alert.alert(
            this.translate('video.uploader.alert.offline.title'),
            this.translate('video.uploader.alert.offline.message'),
        );
    }

    showNotWifiAlert() {
        return Alert.alert(
            this.translate('video.uploader.alert.noWifi.title'),
            this.translate('video.uploader.alert.noWifi.message'),
            [
                {
                    text: this.translate('video.uploader.alert.noWifi.button.quit'),
                    onPress: () => {
                        this.props.onQuit();
                    },
                }, {
                    text: this.translate('video.uploader.alert.noWifi.button.continue'),
                    onPress: () => {
                        this.ignoreWifi = true;
                        this.handleVideo();
                    },
                },
            ],
        );
    }

    getProgressText() {
        const video = this.getCurrentVideo();
        let currentState = video.state;

        if (['new', 'recorded'].includes(currentState)) {
            currentState = 'uploading';
        }

        return this.translate(`video.uploader.state.${currentState}`);
    }

    getOverallProgress() {
        return this.getStep() / 6;
    }

    getCurrentVideo(): Video {
        let { video, videos } = this.props;
        return videos.find(v => v.id === video.id);
    }
}

export const ConnectedVideoUploader = creaceConnect(_ConnectedVideoUploader);
