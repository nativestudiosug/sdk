// @flow
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import { RecordEdit } from '@nativestudios/creace-recorder';
import React, { Component } from 'react';
import type { ConnectedProps } from '../../typings/ConnectedProps';
import { creaceConnect } from '../store';

type Props = {
    video: Video;
    instruction: Instruction;
    onEdit: Function;
    onQuit: Function;
};

export class _ConnectedRecordEdit extends Component<Props & ConnectedProps> {
    render() {
        const { video, instruction, records, settings } = this.props;
        const record = records.find((record: Record) => record.instruction_id === instruction.id && record.video_id === video.id);

        return (
            <RecordEdit instruction={instruction}
                        record={record}
                        showCountdown={settings.countdown !== 'disabled'}
                        onRecord={this.onRecord.bind(this)}
                        onQuit={this.props.onQuit}/>
        );
    }

    onRecord(value: string) {
        const { video, instruction } = this.props;

        const newRecord: Record = {
            instruction_id: instruction.id,
            video_id: video.id,
            value: value,
            state: 'new',
            created_at: Date.now(),
        };

        this.props.addOrUpdateRecord(newRecord);

        this.props.onEdit(video, instruction);
    }
}

export const ConnectedRecordEdit = creaceConnect(_ConnectedRecordEdit);
