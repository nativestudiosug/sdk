import { CATEGORIES_SET } from '../actions/categories';

export function categories(state = [], action: any) {
    switch (action.type) {
        case CATEGORIES_SET:
            return action.data;
    }

    return state;
}
