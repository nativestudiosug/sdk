// @flow
import type { Record } from '@nativestudios/creace-core/typings/Record';
import type { Video } from '@nativestudios/creace-core/typings/Video';
import { VIDEO_DELETE } from '../actions';
import { RECORD_ADD_OR_UPDATE } from '../actions/records';

type RecordAction = {
    type: string;
    data: Record | Video;
};

export function records(state = [], action: RecordAction) {
    switch (action.type) {
        case RECORD_ADD_OR_UPDATE:
            let found = false;
            const newState = state.map((record: Record) => {
                if (record.video_id !== action.data.video_id) {
                    return record;
                }

                if (record.instruction_id !== action.data.instruction_id) {
                    return record;
                }

                found = true;

                return action.data;
            });

            if (!found) {
                newState.push(action.data);
            }

            return newState;
        case VIDEO_DELETE:
            return [
                ...state
            ].filter((record: Record) => record.video_id !== action.data.id);
    }

    return state;
}
