// @flow
import type { Template } from '@nativestudios/creace-core/typings/Template';
import { TEMPLATES_ADD, TEMPLATES_SET } from '../actions/templates';

type Action = {
    type: string;
    data: Array<Template>;
};

export function templates(state = [], action: Action) {
    switch (action.type) {
        case TEMPLATES_SET:
            return action.data;
        case TEMPLATES_ADD:
            const newState = [
                ...state,
            ];

            for (const template of action.data) {
                if (newState.find(t => t.id === template.id)) {
                    continue;
                }

                newState.push(template);
            }

            return newState;
    }

    return state;
}
