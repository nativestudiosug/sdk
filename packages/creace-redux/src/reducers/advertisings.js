import { ADVERTISING_SET } from '../actions/advertisings';

export function advertisings(state = [], action: any) {
    switch (action.type) {
        case ADVERTISING_SET:
            return action.data;
    }

    return state;
}
