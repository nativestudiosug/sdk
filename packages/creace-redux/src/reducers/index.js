import { combineReducers } from 'redux';
import { categories } from './categories';
import { advertisings } from './advertisings';
import { device } from './device';
import { templates } from './templates';
import { videos } from './videos';
import { records } from './records';
import { settings } from './settings';

export const reducers = combineReducers({
    advertisings,
    device,
    categories,
    templates,
    videos,
    records,
    settings,
});
