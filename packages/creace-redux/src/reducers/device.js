import { DEVICE_UPDATE } from '../actions/device';

const INITIAL_STATE = {
    token: null,
    push_token: null,
};

export const device = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case DEVICE_UPDATE:
            return Object.assign({}, state, action.data);
        case 'RESET':
            return {
                ...INITIAL_STATE,
            };
        default:
            return state;
    }
};
