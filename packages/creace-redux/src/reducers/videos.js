// @flow
import type { Video } from '@nativestudios/creace-core/typings/Video';
import { VIDEO_ADD_OR_UPDATE, VIDEO_DELETE } from '../actions/videos';

type VideoAction = {
    type: string;
    data: Video;
};

export function videos(state = [], action: VideoAction) {
    switch (action.type) {
        case VIDEO_ADD_OR_UPDATE:
            let found = false;
            const newState = state.map(video => {
                if (video.id !== action.data.id) {
                    return video;
                }

                found = true;

                return action.data;
            });

            if (!found) {
                newState.push(action.data);
            }

            return newState;
        case VIDEO_DELETE:
            return [
                ...state
            ].filter((video: Video) => video.id !== action.data.id);
    }

    return state;
}
