// @flow
import { SETTINGS_SET } from '../actions/settings';

type Action = {
    type: string;
    name: string;
    value: string;
};

export const settings = (state = {}, action: Action) => {
    switch (action.type) {
        case SETTINGS_SET:
            return {
                ...state,
                [action.name]: action.value,
            };
        default:
            return state;
    }
};
