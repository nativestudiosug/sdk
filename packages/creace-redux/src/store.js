// @flow
import { connect } from 'react-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import { persistReducer, persistStore } from 'redux-persist';
import stateReconciler from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import storage from '@react-native-community/async-storage';
import thunk from 'redux-thunk';
import * as actions from './actions';
import { reducers } from './reducers';

const persistConfig = {
    key: 'creace',
    timeout: 10000,
    storage,
    stateReconciler,
};

const loggerOptions = {
    timestamp: true,
};

const middlewares = __DEV__ ?
    [thunk, createLogger(loggerOptions)] :
    [thunk];

const enhancers = [];

if (__DEV__ && typeof __REDUX_DEVTOOLS_EXTENSION__ === 'function') {
    enhancers.push(__REDUX_DEVTOOLS_EXTENSION__());
}

const composedEnhancers = compose(
    applyMiddleware(...middlewares),
    ...enhancers,
);

export function configureStore() {
    const persistedReducer = persistReducer(persistConfig, reducers);
    const store = createStore(
        persistedReducer,
        composedEnhancers,
    );
    const persistor = persistStore(store);

    return { store, persistor };
}

export const mapStateToProps = ({ device, categories, templates, videos, records, settings, advertisings }) =>
    ({ device, categories, templates, videos, records, settings, advertisings });

export const mapDispatchToProps = (dispatch) => ({
    setAdvertisings: (...args) => dispatch(actions.setAdvertisings(...args)),
    setSetting: (...args) => dispatch(actions.setSetting(...args)),
    updateDevice: (...args) => dispatch(actions.updateDevice(...args)),
    setCategories: (...args) => dispatch(actions.setCategories(...args)),
    setTemplates: (...args) => dispatch(actions.setTemplates(...args)),
    addTemplates: (...args) => dispatch(actions.addTemplates(...args)),
    addOrUpdateVideo: (...args) => dispatch(actions.addOrUpdateVideo(...args)),
    deleteVideo: (...args) => dispatch(actions.deleteVideo(...args)),
    addOrUpdateRecord: (...args) => dispatch(actions.addOrUpdateRecord(...args)),
});

export function creaceConnect(component) {
    return connect(mapStateToProps, mapDispatchToProps)(component);
}
