# `@nativestudios/creace-redux`

Integrates redux with persistence and logging.
Also, provides "store connected" components wrapping components from `@nativestudios/creace-ui`
