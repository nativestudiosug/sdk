# `@nativestudios/creace-core`

The core package which handles the communication with the API and provides a React Context which can be used by components.
