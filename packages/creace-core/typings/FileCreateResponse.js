import type { File  } from './File';

export type FileCreateResponse = {
    file: File,
    method: string;
    url: string;
};
