// @flow
export type Category = {
    id: number;
    priority: number;
    height: number;
    name: string;
    preview_image: string;
    teaser_image: string;
    type: string;
    text_position: string;
};
