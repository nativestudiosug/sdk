import { Context } from '../lib/context/Context';

export type Config = {
    api_base_url?: string;
    video_url?: string;
    token?: string;
    context?: Context;
}
