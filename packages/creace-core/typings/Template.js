// @flow

import { Instruction } from './Instruction';

export type SharingData = {
    button_text: string;
    visit_url?: string;
    share_url?: string;
    duplicate_template_id?: number;
};

export type Sharing = {
    share_with_friends?: SharingData;
    camera_roll?: SharingData;
    extra?: SharingData;
    duplicate_template?: SharingData;
};

export type Template = {
    id: number;
    categories: Array<number>;
    priority: number;
    version: number;
    requires_geo: number;
    is_teaser: number;
    name: string;
    preview_image: string;
    preview_video: string;
    preview_title: string;
    preview_description: string;
    preview_cta: string;
    processing_screen: string;
    processing_button: string;
    teaser_image: string;
    sharing_text: string;
    sharing: Sharing;
    video_advertisement: string;
    instructions: Array<Instruction>;
};
