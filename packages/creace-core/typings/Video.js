// @flow
export type Video = {
    id: number;
    template: {
        id: number;
        name: string;
    };
    uid: ?string;
    state: string;
    url: ?string;
    preview_image: ?string;
    created_at: ?string;
    originalVideo?: number;
};
