import { Analytics, ErrorHandler, Theme, Translator } from '../lib/context';

export type ContextOptions = {
    theme?: Theme;
    translator?: Translator;
    errorHandler?: ErrorHandler;
    analytics?: Analytics;
};
