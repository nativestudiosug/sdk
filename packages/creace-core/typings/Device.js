// @flow
export type Device = {
    id: string;
    manufacturer: string;
    model: string;
    system_name: string;
    system_version: string;
    bundle_id: string;
    version: string;
    locale: string;
    country: string;
    push_token?: ?string;
    token?: ?string;
};
