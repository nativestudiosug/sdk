// @flow
export type Advertising = {
    language: string;
    url: string;
};
