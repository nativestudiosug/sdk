export type Instruction = {
    id: number;
    position: number;
    length: number;
    minlength: number;
    type: string;
    instruction_text: string;
    property_name: string;
    camera: 'front' | 'back';
};
