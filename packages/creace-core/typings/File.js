// @flow
export type File = {
    id: number;
    filename: string;
    type: string;
};
