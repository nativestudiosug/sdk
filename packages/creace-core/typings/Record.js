export type Record = {
    video_id: number;
    instruction_id: number;
    file_id: ?number;
    value: string;
    state: 'new' | 'uploaded';
    created_at: Date;
};
