export function stringReplace(original: string, obj: any, prefix: string = '') {
    let replacedString = original;

    for (const property in obj) {
        if (!obj.hasOwnProperty(property)) {
            continue;
        }

        const value = obj[property];

        if (typeof value === 'object') {
            replacedString = stringReplace(replacedString, value, `${prefix}${property}.`)
        }

        console.log(`replacing {${prefix}${property}} with ${value}`);

        replacedString = replacedString.replace(`{${prefix}${property}}`, value);
    }

    return replacedString;
}
