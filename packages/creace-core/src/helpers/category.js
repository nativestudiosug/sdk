// @flow
import type { Category } from '../../typings/Category';
import type { Template } from '../../typings/Template';

export function getCategoriesWithTemplates(categories: Array<Category>, templates: Array<Template>) {
    return categories.filter((category) => {
        return templates.find((template) => {
            return template.categories.find(id => id === category.id);
        });
    });
}
