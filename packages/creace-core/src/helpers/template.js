// @flow
import type { Category } from '../../typings/Category';
import type { Template } from '../../typings/Template';

export function getTemplatesByCategory(templates: Array<Template>, category: Category) {
    return templates.filter((template) => {
        return template.categories.find(id => id === category.id);
    });
}
