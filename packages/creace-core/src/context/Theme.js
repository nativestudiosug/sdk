// @flow
import { Platform, StyleSheet, ViewPropTypes } from 'react-native';

export class Theme {
    borderRadius: number = 10;

    colors = {
        primary: 'rgba(65, 170, 208, 1)',
        secondary: 'rgba(65, 170, 208, 0.4)',
        warning: 'rgba(238, 114, 32, 1)',
        light: 'white',
        dark: 'black',
    };

    fonts = {
        regular: 'Montserrat-Regular',
        bold: 'Montserrat-Bold',
    };

    textStyles: {
        common: ViewPropTypes.style;
        headline: ViewPropTypes.style;
        danger: ViewPropTypes.style;
    } = {
        common: {
            fontFamily: this.fonts.regular,
            fontSize: 16,
            color: this.colors.light,
            textAlign: 'left',
        },
        headline: {
            fontFamily: this.fonts.bold,
            fontSize: 20,
            color: this.colors.light,
            textAlign: 'left',
        },
        danger: {
            fontFamily: this.fonts.regular,
            fontSize: 16,
            color: this.colors.warning,
            textAlign: 'left',
        },
    };

    pageTheme: ViewPropTypes.style = {
        flex: 1,
        backgroundColor: this.colors.dark,
    };

    centeredPageTheme: ViewPropTypes.style = {
        ...this.pageTheme,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
    };

    buttonTheme: {
        container: ViewPropTypes.style;
        text: ViewPropTypes.style;
    } = {
        container: {
            backgroundColor: this.colors.primary,
            borderRadius: this.borderRadius,
            padding: 12,
        },
        text: {
            ...this.textStyles.headline,
            textAlign: 'center',
            color: this.colors.light,
            textTransform: 'uppercase',
        },
    };

    categoryListItemTheme: {
        text: ViewPropTypes.style;
        image: ViewPropTypes.style;
    } = {
        text: {
            ...this.textStyles.headline,
            paddingLeft: 20,
            paddingRight: 20,
        },
        image: {
            justifyContent: 'center',
        },
    };

    countdownTheme: {
        container: ViewPropTypes.style;
        text: ViewPropTypes.style;
    } = {
        container: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
        },
        text: {
            ...this.textStyles.headline,
            fontSize: 120,
        },
    };

    recordListItemTheme: {
        container: ViewPropTypes.style;
        contentContainer: ViewPropTypes.style;
        buttonContainer: ViewPropTypes.style;
        text: ViewPropTypes.style;
        button: ViewPropTypes.style;
    } = {
        container: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            borderBottomWidth: 1,
            borderBottomColor: this.colors.light,
        },
        contentContainer: {
            flex: 1,
            padding: 10,
        },
        buttonContainer: {
            flex: 1,
            padding: 10,
            flexDirection: 'column',
        },
        text: {
            ...this.textStyles.headline,
            fontSize: 14,
        },
        button: {
            marginBottom: 10,
        },
    };

    templateDetailTheme: {
        container?: ViewPropTypes.style;
        image?: ViewPropTypes.style;
        content?: ViewPropTypes.style;
        buttons?: ViewPropTypes.style;
        button?: ViewPropTypes.style;
    } = {
        container: {
            ...this.pageTheme,
        },
        image: {
            flex: 1,
            padding: 10,
        },
        content: {
            paddingLeft: 60,
            paddingRight: 60,
            paddingTop: 10,
        },
        buttons: {
            flexShrink: 1,
            padding: 20,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        button: {
            marginLeft: 10,
            marginRight: 10,
        },
    };

    templateListItemTheme: {
        text: ViewPropTypes.style;
        image: ViewPropTypes.style;
    } = {
        text: {
            ...this.textStyles.headline,
            paddingLeft: 20,
            paddingRight: 20,
        },
        image: {
            height: 250,
            justifyContent: 'center',
            alignItems: 'center',
        },
    };

    videoListItemTheme: {
        container: ViewPropTypes.style;
        leftContainer: ViewPropTypes.style;
        image: ViewPropTypes.style;
        rightContainer: ViewPropTypes.style;
        button: ViewPropTypes.style;
        createdAt: ViewPropTypes.style;
        templateName: ViewPropTypes.style;
    } = {
        container: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingTop: 10,
            paddingBottom: 10,
        },
        leftContainer: {
            flex: 1,
            padding: 10,
        },
        rightContainer: {
            flex: 1,
            padding: 10,
            flexDirection: 'column',
        },
        image: {
            padding: 20,
            flexDirection: 'column',
            justifyContent: 'flex-start',
        },
        button: {
            marginBottom: 10,
        },
        createdAt: {
            ...this.textStyles.common,
            fontSize: 10,
        },
        templateName: {
            ...this.textStyles.headline,
            marginBottom: 10,
        },
    };

    videoPlayerTheme: {
        container: ViewPropTypes.style;
        absoluteContainer: ViewPropTypes.style;
        videoContainer: ViewPropTypes.style;
        video: ViewPropTypes.style;
        absolute: ViewPropTypes.style;
        controls: ViewPropTypes.style;
        currentTimeText: ViewPropTypes.style;
        durationText: ViewPropTypes.style;
        bar: ViewPropTypes.style;
        barCurrentTime: ViewPropTypes.style;
    } = {
        container: {
            ...this.pageTheme,
        },
        absoluteContainer: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
        },
        videoContainer: {
            flex: 1,
        },
        video: {
            flex: 1,
        },
        controls: {
            backgroundColor: 'rgba(0, 0, 0, 0.2)',
        },
        currentTimeText: {
            ...this.textStyles.common,
            position: 'absolute',
            left: 10,
            bottom: 10,
        },
        durationText: {
            ...this.textStyles.common,
            position: 'absolute',
            right: 10,
            bottom: 10,
        },
        bar: {
            position: 'absolute',
            bottom: 50,
            left: 10,
            right: 10,
            height: 2,
            backgroundColor: this.colors.secondary,
        },
        barCurrentTime: {
            position: 'absolute',
            bottom: 50,
            left: 10,
            height: 2,
            backgroundColor: this.colors.primary,
        },
    };

    videoUploaderTheme: {
        container: ViewPropTypes.style;
        backgroundVideo: ViewPropTypes.style;
        progressContainer: ViewPropTypes.style;
        progressBar: ViewPropTypes.style;
        progressText: ViewPropTypes.style;
        controls: ViewPropTypes.style;
        togglePlay: ViewPropTypes.style;
    } = {
        container: {
            ...this.pageTheme,
            justifyContent: 'center',
            alignItems: 'center',
        },
        backgroundVideo: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
        },
        progressContainer: {
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            height: 60,
            paddingTop: 20,
        },
        progressBar: {
            position: 'absolute',
            left: 0,
            top: 0,
            height: 5,
            backgroundColor: this.colors.primary,
        },
        progressText: {
            ...this.textStyles.common,
            textAlign: 'center',
            flex: 1,
        },
    };

    instructionBarTheme: {
        container: ViewPropTypes.style;
    } = {
        container: {
            position: 'absolute',
            left: 0,
            top: 0,
            width: 60,
            flex: 1,
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
        },
    };

    instructionBarItemTheme: {
        container: ViewPropTypes.style;
        bar: ViewPropTypes.style;
        label: ViewPropTypes.style;
        position: ViewPropTypes.style;
    } = {
        container: {
            position: 'relative',
            height: 80,
            width: 60,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 5,
        },
        bar: {
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            width: 5,
        },
        label: {
            ...this.textStyles.common,
            fontSize: 8,
        },
        position: {
            ...this.textStyles.common,
            fontSize: 20,
        },
    };

    quitQuestionTheme: {
        container: ViewPropTypes.style;
        buttonsContainer: ViewPropTypes.style;
        text: ViewPropTypes.style;
        button: ViewPropTypes.style;
    } = {
        container: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            padding: 30,
            backgroundColor: 'rgba(0, 0, 0, 0.8)',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        buttonsContainer: {
            marginTop: 30,
            flexDirection: 'row',
            justifyContent: 'space-between',
        },
        text: {
            ...this.textStyles.headline,
            textAlign: 'center',
        },
        button: {
            marginLeft: 10,
            marginRight: 10,
            minWidth: 100,
        },
    };

    recordButtonTheme: {
        container: ViewPropTypes.style;
        outerContainer: ViewPropTypes.style;
        innerContainer: ViewPropTypes.style;
    } = {
        container: {
            position: 'absolute',
            top: 0,
            right: 25,
            bottom: 0,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
        },
        outerContainer: {
            position: 'relative',
            backgroundColor: this.colors.primary,
            width: 80,
            height: 80,
            borderRadius: 40,
            justifyContent: 'center',
            alignItems: 'center',
        },
        innerContainer: {
            backgroundColor: this.colors.primary,
            width: 70,
            height: 70,
            borderRadius: 35,
            borderWidth: 3,
            borderColor: 'black',
        },
    };

    buttonsTopRightTheme: ViewPropTypes.style = {
        position: 'absolute',
        top: 0,
        right: 0,
    };

    iconButtonTheme: ViewPropTypes.style = {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: this.colors.primary,
        borderRadius: this.borderRadius,
    };

    redoButtonTheme: {
        container: ViewPropTypes.style;
        text: ViewPropTypes.style;
    } = {
        container: {
            position: 'absolute',
            right: 0,
            bottom: 0,
            padding: 20,
        },
        text: {
            ...this.textStyles.common,
        },
    };

    summaryTheme: {
        safeArea: ViewPropTypes.style;
        container: ViewPropTypes.style;
        markdownContainer: ViewPropTypes.style;
        buttonsContainer: ViewPropTypes.style;
        button: ViewPropTypes.style;
        markdown: {
            text: ViewPropTypes.style;
            heading1: ViewPropTypes.style;
        };
    } = {
        safeArea: {
            position: 'absolute',
            backgroundColor: 'rgba(0, 0, 0, 0.8)',
            left: 0,
            top: 0,
            bottom: 0,
            right: 0,
        },
        container: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        markdownContainer: {
            paddingLeft: 90,
            paddingRight: 90,
            justifyContent: 'center',
            alignItems: 'center',
        },
        buttonsContainer: {
            marginTop: 20,
            flexDirection: 'row',
        },
        button: {
            marginLeft: 10,
            marginRight: 10,
        },
        markdown: {
            text: {
                ...this.textStyles.common,
                textAlign: 'center',
                fontSize: 16,
            },
            heading1: {
                ...this.textStyles.headline,
                textAlign: 'center',
                fontSize: 20,
                flex: 1,
            },
        },
    };

    textInstructionTheme: {
        container: ViewPropTypes.style;
        containerHotZone: ViewPropTypes.style;
        instructionText: ViewPropTypes.style;
        textInput: ViewPropTypes.style;
    } = {
        container: {
            position: 'absolute',
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            backgroundColor: 'rgba(0, 0, 0, 0.8)',
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'center',
            opacity: 0.8,
        },
        containerHotZone: {
            position: 'absolute',
            left: 60,
            top: 60,
            right: 0,
            bottom: 0,
        },
        instructionText: {
            ...this.textStyles.common,
            textAlign: 'center',
            marginLeft: 130,
            marginRight: 130,
            marginTop: 80,
        },
        textInput: {
            marginTop: 20,
            color: Platform.OS === 'ios' ? 'white' : 'black',
            fontSize: 20,
        },
    };

    videoInstructionTheme: {
        container: ViewPropTypes.style;
        instructionContainer: ViewPropTypes.style;
        containerHotZone: ViewPropTypes.style;
        instructionText: ViewPropTypes.style;
    } = {
        container: {
            position: 'absolute',
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
        },
        instructionContainer: {
            position: 'absolute',
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 20,
            paddingBottom: 20,
        },
        containerHotZone: {
            position: 'absolute',
            left: 60,
            top: 60,
            right: 0,
            bottom: 0,
        },
        instructionText: {
            ...this.textStyles.common,
            textAlign: 'center',
            marginLeft: 130,
            marginRight: 130,
        },
    };

    cameraSwitchButtonTheme: {
        container: ViewPropTypes.style;
        button: ViewPropTypes.style;
    } = {
        container: {
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            alignItems: 'center',
        },
        button: {
            padding: 20,
        },
    };

    markdownTheme = StyleSheet.create({
        heading1: {
            ...this.textStyles.headline,
            fontSize: 24,
            marginBottom: 20,
        },
        heading2: {
            ...this.textStyles.headline,
            marginBottom: 10,
        },
        text: {
            ...this.textStyles.common,
        },
        strong: {
            ...this.textStyles.common,
            fontSize: 12,
        },
    });

    videoProgressBarTheme: {
        container: ViewPropTypes.style;
        progressBar: ViewPropTypes.style;
    } = {
        container: {
            position: 'absolute',
            left: 0,
            top: 0,
            bottom: 0,
            width: 60,
            flex: 1,
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
        },
        progressBar: {
            position: 'absolute',
            right: 0,
            bottom: 0,
            width: 5,
        },
    };

    videoDeleteQuestonTheme: {
        container: ViewPropTypes.style;
        buttonsContainer: ViewPropTypes.style;
        text: ViewPropTypes.style;
        button: ViewPropTypes.style;
    } = {
        container: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            padding: 30,
            backgroundColor: 'rgba(0, 0, 0, 0.8)',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        buttonsContainer: {
            marginTop: 30,
            flexDirection: 'row',
            justifyContent: 'space-between',
        },
        switchContainer: {
            marginTop: 10,
            flexDirection: 'row',
            width: '100%',
        },
        text: {
            ...this.textStyles.headline,
            textAlign: 'center',
        },
        button: {
            marginLeft: 10,
            marginRight: 10,
            minWidth: 100,
        },
    };

}
