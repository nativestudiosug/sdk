// @flow
import React, { Component } from 'react';
import { Analytics } from './Analytics';
import { Context, CreaceContext } from './Context';
import { ErrorHandler } from './ErrorHandler';
import { Theme } from './Theme';
import { Translator } from './Translator';

export class ContextualComponent<Props, State> extends Component<Props, State> {
    static contextType = CreaceContext;

    get creaceContext(): Context {
        if (!(this.context instanceof Context)) {
            console.log(this.context);
            throw new Error('CreaceContext should be an instance of Context');
        }

        return this.context;
    }

    get analytics(): Analytics {
        return this.creaceContext.analytics;
    }

    get theme(): Theme {
        return this.creaceContext.theme;
    }

    get translator(): Translator {
        return this.creaceContext.translator;
    }

    get errorHandler(): ErrorHandler {
        return this.creaceContext.errorHandler;
    }

    translate(text: string): string {
        return this.translator.translate(text);
    }
}
