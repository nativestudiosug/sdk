export class ErrorHandler {
    notifyError(error: any) {
        if (__DEV__) {
            console.error(error);
        }
    }
}
