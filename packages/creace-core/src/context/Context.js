// @flow
import { createContext } from 'react';
import type { ContextOptions } from '../../typings/ContextOptions';
import { Analytics } from './Analytics';
import { ErrorHandler } from './ErrorHandler';
import { Theme } from './Theme';
import { Translator } from './Translator';

export class Context {
    theme: Theme;
    translator: Translator;
    errorHandler: ErrorHandler;
    analytics: Analytics;

    constructor(options: ContextOptions = {}) {
        this.theme = options.theme || new Theme();
        this.translator = options.translator || new Translator();
        this.errorHandler = options.errorHandler || new ErrorHandler();
        this.analytics = options.analytics || new Analytics();

        if (typeof CREACE_CONFIG !== 'undefined') {
           CREACE_CONFIG.setContext(this);
        }
    }
}

export const CreaceContext = createContext(new Context());
