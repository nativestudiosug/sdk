// @flow
import type { Config } from '../typings/Config';
import { Context } from './context/Context';

export class CreaceConfig {
    config: Config;

    constructor(config: Config) {
        this.config = config;
    }

    getApiBaseUrl(): string {
        return this.config.api_base_url || '';
    }

    getVideoUrl(): string {
        return this.config.video_url || '';
    }

    getToken(): ?string {
        return this.config.token;
    }

    setToken(value: string): void {
        this.config.token = value;
    }

    getContext(): Context {
        return this.config.context;
    }

    setContext(context: Context): void {
        this.config.context = context;
    }
}

export function createConfig(config: Config) {
    global.CREACE_CONFIG = new CreaceConfig(config);
}
