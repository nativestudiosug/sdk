import { Platform } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';

const { fs } = RNFetchBlob;
const { dirs } = fs;

export class File {
    path: string;

    constructor(path: string) {
        if (Platform.OS === 'ios') {
            const rootPath = dirs.DocumentDir.replace('/Documents', '');
            path = path.replace('file://', '');
            const parts = path.split('/');
            const rootPathParts = rootPath.split('/');
            parts.splice(0, rootPathParts.length, ...rootPathParts);
            path = parts.join('/');
        }
        this.path = path;
    }

    get basename(): string {
        return this.path.split('/').pop();
    }

    get extension(): string {
        return this.basename.split('.').pop();
    }

    get mimeType(): string {
        switch (this.extension) {
            case 'mpeg':
            case 'mpg':
            case 'mpe':
                return 'video/mpeg';
            case 'qt':
            case 'mov':
                return 'video/quicktime';
            case 'viv':
            case 'vivo':
                return 'video/vnd.vivo';
            case 'avi':
                return 'video/x-msvideo';
            case 'movie':
                return 'video/x-sgi-movie';
            case 'mp4':
                return 'video/mp4';
            default:
                return 'video/unknown';
        }
    }

    async exists(): Promise<boolean> {
        return await fs.exists(this.path);
    }

    async delete() {
        return await fs.unlink(this.path);
    }
}
