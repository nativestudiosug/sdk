// @flow
import RNFetchBlob from 'rn-fetch-blob';
import type { Instruction } from '../../typings/Instruction';
import type { Record } from '../../typings/Record';
import type { Template } from '../../typings/Template';
import type { Video } from '../../typings/Video';
import { CreaceConfig } from '../Config';
import { File } from '../file/File';
import { stringReplace } from '../helpers/replace';
import { Api } from './Api';
import { FileApi } from './FileApi';

declare var CREACE_CONFIG: CreaceConfig;

class _VideoApi extends Api {
    async create(template: Template): Promise<Video> {
        const result = await this.post('/videos', {
            template_id: template.id,
            version: template.version,
        });

        if (!result || !result.data) {
            console.log(result);
            throw new Error('Unexpected result');
        }

        return this._mapResultToVideo(result);
    }

    async load(videoId: number): Promise<Video> {
        const result = await this.get(`/videos/${videoId}`);

        if (!result || !result.data) {
            throw new Error('Unexpected result');
        }

        return this._mapResultToVideo(result);
    }

    async render(video: Video) {
        await this.get(`/videos/${video.id}/render`);
    }

    async destroy(videoId: number): Promise<void> {
        await this.delete(`/videos/${videoId}`);
    }

    uploadRecord(video: Video, instruction: Instruction, record: Record): Promise<File> {
        let uploadProgressCallback;
        const uploadProgress = (callback: Function) => uploadProgressCallback = callback;

        const promise = new Promise(async (resolve, reject) => {
            if (record.state === 'uploaded') {
                return resolve(null);
            }

            if (instruction.type === 'text') {
                const result = await this.addProperty(video, instruction, record.value);
                return resolve(result);
            }

            try {
                const file = new File(record.value);
                const response = await FileApi.create(file);
                await RNFetchBlob
                    .fetch(response.method, response.url, {
                        'Content-Type': file.mimeType,
                    }, RNFetchBlob.wrap(file.path))
                    .uploadProgress((...args) => {
                        if (typeof uploadProgressCallback === 'function') {
                            uploadProgressCallback(...args);
                        }
                    });
                await this.post(`/videos/${video.id}/files`, {
                    file_id: response.file.id,
                    template_instruction_id: record.instruction_id,
                });

                resolve(response.file);
            } catch (error) {
                reject(error);
            }
        });

        promise.uploadProgress = uploadProgress;

        return promise;
    }

    async addProperty(video: Video, instruction: Instruction, value: string) {
        try {
            await this.post(`/videos/${video.id}/properties`, {
                name: instruction.property_name,
                value,
            });

            return true;
        } catch (error) {
            CREACE_CONFIG.getContext().errorHandler.notifyError(error);
        }

        return false;
    }

    _mapResultToVideo(result: any): Video {
        return {
            id: result.data.id,
            template: result.data.template,
            uid: result.data.uid,
            state: result.data.state || 'new',
            url: result.data.url,
            preview_image: result.data.preview_image,
            created_at: result.data.created_at,
        };
    }

    getVideoUrl(video: Video) {
        return stringReplace(CREACE_CONFIG.getVideoUrl(), video);
    }
}

export const VideoApi = new _VideoApi();
