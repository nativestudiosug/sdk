// @flow
import type { Category } from '../../typings/Category';
import { Api } from './Api';

class _CategoryApi extends Api {
    async fetch() {
        const result = await this.get('/categories');

        if (!result || !result.data) {
            return [];
        }

        return result.data.map((category: Category) => {
            return {
                ...category,
                preview_image: category.preview_image ? category.preview_image.replace(/ /g, '%20') : category.preview_image,
                teaser_image: category.teaser_image ? category.teaser_image.replace(/ /g, '%20') : category.teaser_image,
            }
        });
    }
}

export const CategoryApi = new _CategoryApi();
