// @flow
import { CreaceConfig } from '../Config';

declare var CREACE_CONFIG: CreaceConfig;

export class Api {
    getBaseHeaders(): Headers {
        const headers = new Headers();
        const token: string = CREACE_CONFIG.getToken() || '';

        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');

        if (token.length) {
            headers.append('Authorization', `Bearer ${token}`);
        }

        return headers;
    }

    async get(resource: string, headers: ?Headers) {
        return await this.request(resource, 'GET', null, headers);
    }

    async post(resource: string, body: any, headers: ?Headers) {
        return await this.request(resource, 'POST', body, headers);
    }

    async delete(resource: string, headers: ?Headers) {
        return await this.request(resource, 'DELETE', null, headers);
    }

    async request(resource: string, method: 'GET' | 'POST' | 'DELETE' = 'GET', body: any = null, headers: ?Headers) {
        const baseHeaders = this.getBaseHeaders();
        headers = headers || new Headers();

        for (const [key, value] of baseHeaders.entries()) {
            if (headers.has(key)) {
                continue;
            }

            headers.set(key, value);
        }

        const options: any = {
            method,
            headers,
        };

        if (body) {
            options.body = JSON.stringify(body);
        }

        const response = await fetch(`${CREACE_CONFIG.getApiBaseUrl()}${resource}`, options);
        if (response.status === 204) {
            return true;
        }

        try {
            return await response.json();
        } catch (error) {
            const context = CREACE_CONFIG.getContext();

            if (context && context.errorHandler) {
                context.errorHandler.notifyError(error);
            }
        }

        return response;
    }
}
