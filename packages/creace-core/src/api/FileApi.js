// @flow
import type { FileCreateResponse } from '../../typings/FileCreateResponse';
import { File } from '../file/File';
import { Api } from './Api';

class _FileApi extends Api {
    async create(file: File): Promise<FileCreateResponse> {
        return await this.post('/files?use=url', {
            filename: file.basename,
            type: 'video',
        });
    }
}

export const FileApi = new _FileApi();
