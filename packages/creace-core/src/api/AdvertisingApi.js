// @flow
import type { Advertising } from '../../typings/Advertising';
import { Api } from './Api';

class _AdvertisingApi extends Api {
    async fetch(): Promise<Array<Advertising>> {
        const result = await this.get('/advertisings');

        if (!result || !result.data) {
            return [];
        }

        return result.data;
    }
}

export const AdvertisingApi = new _AdvertisingApi();
