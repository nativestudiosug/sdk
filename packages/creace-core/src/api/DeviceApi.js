// @flow
import DeviceInfo from 'react-native-device-info';
import * as RNLocalize from 'react-native-localize';
import { CreaceConfig } from '../Config';
import { Api } from './Api';
import type { Device } from '../../typings/Device';

declare var CREACE_CONFIG: CreaceConfig;

class _DeviceApi extends Api {
    async getDefaultData(): Device {
        const locales = RNLocalize.getLocales();
        const locale = !locales.length ? 'en' : locales[0].languageCode;

        return {
            id: await DeviceInfo.getUniqueId(),
            manufacturer: await DeviceInfo.getManufacturer(),
            model: await DeviceInfo.getModel(),
            system_name: await DeviceInfo.getSystemName(),
            system_version: await DeviceInfo.getSystemVersion(),
            bundle_id: await DeviceInfo.getBundleId(),
            version: await DeviceInfo.getReadableVersion(),
            locale: locale,
            country: RNLocalize.getCountry().toUpperCase(),
        };
    }

    async register(data?: Device = {}): Promise<Device> {
        const defaultData = await this.getDefaultData();
        const device: Device = Object.assign({}, defaultData, data);
        device.locale = device.locale.split('-')[0];

        const body = {
            id: device.id,
            manufacturer: device.manufacturer,
            model: device.model,
            system_name: device.system_name,
            system_version: device.system_version,
            bundle_id: device.bundle_id,
            version: device.version,
            locale: device.locale,
            country: device.country,
            push_token: device.push_token,
        };
        const result = await this.post('/devices', body);

        if (!result || !result.token) {
            console.error('Unexpected result');
        }

        device.token = result.token;
        CREACE_CONFIG.setToken(result.token);

        return device;
    }
}

export const DeviceApi = new _DeviceApi();
