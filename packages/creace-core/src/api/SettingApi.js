// @flow
import { Api } from './Api';
import type { Setting } from '../../typings/Setting';

class _SettingApi extends Api {
    async fetch(): Promise<Array<Setting>> {
        const result = await this.get('/settings');

        if (!result || !result.data) {
            return [];
        }

        return result.data;
    }
}

export const SettingApi = new _SettingApi();
