// @flow
import type { Template } from '../../typings/Template';
import { Api } from './Api';

class _TemplateApi extends Api {
    async fetch(): Promise<Array<Template>> {
        const result = await this.get('/templates');

        if (!result || !result.data) {
            return [];
        }

        return result.data.map(this.mapResultToTemplate);
    }

    async submitCode(code: string): Promise<Array<Template>> {
        const result = await this.post('/codes', { code });

        if (!result || !result.data) {
            return [];
        }

        return result.data;
    }

    mapResultToTemplate(template: Template): Template {
        return {
            ...template,
            preview_image: template.preview_image ? template.preview_image.replace(/ /g, '%20') : template.preview_image,
            preview_video: template.preview_video ? template.preview_video.replace(/ /g, '%20') : template.preview_video,
            teaser_image: template.teaser_image ? template.teaser_image.replace(/ /g, '%20') : template.teaser_image,
            video_advertisement: template.video_advertisement ? template.video_advertisement.replace(/ /g, '%20') : template.video_advertisement,
        };
    }
}

export const TemplateApi = new _TemplateApi();
