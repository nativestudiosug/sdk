// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import { Button } from '@nativestudios/creace-ui';
import React from 'react';
import { Text, View } from 'react-native';

type Props = {
    onAccept: Function;
    onAbort: Function;
}

export class QuitQuestion extends ContextualComponent<Props> {
    render() {
        const { onAccept, onAbort } = this.props;
        const { container, text, button, buttonsContainer } = this.theme.quitQuestionTheme;

        return (
            <View style={[container]}>
                <Text style={text}>{this.translate('quit.question.text')}</Text>
                <View style={[buttonsContainer]}>
                    <Button style={[button]} text={this.translate('quit.question.button.accept')} onPress={onAccept}/>
                    <Button style={[button]} text={this.translate('quit.question.button.abort')} onPress={onAbort}/>
                </View>
            </View>
        );
    }
}
