// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import React from 'react';
import { Dimensions, View } from 'react-native';
import { InstructionsBarItem } from './InstructionsBarItem';

type Props = {
    instructions: Array<Instruction>;
    currentInstruction: number;
};

export class InstructionsBar extends ContextualComponent<Props> {
    render() {
        const { height } = Dimensions.get('window');
        const bottom = height / 2 - 40 - this.props.currentInstruction * 80;
        const summaryInstruction: Instruction = {
            position: this.props.instructions.length + 1,
            type: 'summary',
        };
        const type = this.props.instructions.length === this.props.currentInstruction ? 'current' : 'upcoming';

        return (
            <View style={[this.theme.instructionBarTheme.container, { bottom }]}>
                <InstructionsBarItem instruction={summaryInstruction} type={type} />
                {this.renderInstructions()}
            </View>
        );
    }

    renderInstructions() {
        const instructions = [
            ...this.props.instructions,
        ];

        return instructions
            .sort((a, b) => a.position - b.position)
            .map((instruction, index) => {
                let type = 'upcoming';

                if (index === this.props.currentInstruction) {
                    type = 'current';
                }

                if (index < this.props.currentInstruction) {
                    type = 'done';
                }

                return <InstructionsBarItem instruction={instruction} key={instruction.id.toString()} type={type}/>;
            })
            .reverse();
    }
}
