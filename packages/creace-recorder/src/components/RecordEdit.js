// @flow
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import { IconButton } from '@nativestudios/creace-ui';
import React from 'react';
import { Alert, View } from 'react-native';
import { TextInstruction } from './TextInstruction';
import { VideoInstruction } from './VideoInstruction';

type Props = {
    instruction: Instruction;
    record?: Record;
    onRecord: Function;
    onQuit: Function;
    showCountdown?: boolean;
};

export class RecordEdit extends ContextualComponent<Props> {
    static defaultProps: Props = {
        showCountdown: true,
        record: null,
    };

    render() {
        return (
            <View style={[this.theme.pageTheme]}>
                {this.renderInstruction()}
                <View style={this.theme.buttonsTopRightTheme}>
                    <IconButton onPress={this.props.onQuit} icon={faTimes} style={{ backgroundColor: 'transparent' }}/>
                </View>
            </View>
        );
    }

    renderInstruction() {
        const { record, instruction, showCountdown } = this.props;

        if (instruction.type === 'text') {
            return <TextInstruction instruction={instruction}
                                    initialValue={record ? record.value : ''}
                                    onValue={this.onValue.bind(this)}/>;
        }

        return <VideoInstruction instruction={instruction}
                                 showCountdown={showCountdown}
                                 onCountdownStarted={() => this.setState({ isRecording: true })}
                                 onCountdownStopped={() => this.setState({ isRecording: true })}
                                 onRecordStarted={() => this.setState({ isRecording: true })}
                                 onRecordFinished={() => this.setState({ isRecording: false })}
                                 onError={this.onError.bind(this)}
                                 onValue={this.onValue.bind(this)}/>;
    }

    onError() {
        Alert.alert(
            this.translate('record.error.alert.title'),
            this.translate('record.error.alert.message'),
        );
    }

    onValue(value: string) {
        this.props.onRecord(value);
    }
}
