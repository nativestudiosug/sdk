// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import React from 'react';
import { Text, TextInput, TouchableWithoutFeedback, View } from 'react-native';

type Props = {
    instruction: Instruction;
    initialValue?: string;
    onValue: Function;
};

type State = {
    showTextInput: boolean;
    text: string;
};

export class TextInstruction extends ContextualComponent<Props, State> {
    static defaultProps: Props = {
        initialValue: '',
    };

    state: State;

    constructor(props) {
        super(props);

        this.state = {
            showTextInput: false,
            text: this.props.initialValue,
        };
    }

    render() {
        const instruction: Instruction = this.props.instruction;
        const { container, containerHotZone, instructionText } = this.theme.textInstructionTheme;

        return (
            <View style={[container]}>
                <TouchableWithoutFeedback onPress={this.showTextInput.bind(this)}>
                    <View style={containerHotZone}/>
                </TouchableWithoutFeedback>
                <Text style={[instructionText, { marginTop: this.state.showTextInput ? 20 : instructionText.marginTop }]}>
                    {instruction.instruction_text}
                </Text>
                {this.renderTextInput()}
            </View>
        );
    }

    renderTextInput() {
        if (!this.state.showTextInput) {
            return (
                <Text style={[this.theme.textInstructionTheme.textInput, { color: 'white' }]}>
                    {this.state.text}
                </Text>
            );
        }

        return <TextInput
            autoFocus={true}
            style={this.theme.textInstructionTheme.textInput}
            returnKeyType="send"
            value={this.state.text}
            onChangeText={(text) => this.setState({ text })}
            onSubmitEditing={() => {
                this.props.onValue(this.state.text);
                this.setState({ text: '', showTextInput: false });
            }}
        />;
    }

    showTextInput() {
        this.setState({
            showTextInput: true,
        });
    }
}
