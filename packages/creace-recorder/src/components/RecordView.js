// @flow
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { ContextualComponent, File } from '@nativestudios/creace-core';
import type { Record } from '@nativestudios/creace-core/typings/Record';
import { IconButton, VideoPlayer } from '@nativestudios/creace-ui';
import React from 'react';
import { View } from 'react-native';

type Props = {
    record: Record;
    onQuit?: Function;
    onEnd?: Function;
    onError?: Function;
};

type State = {};

export class RecordView extends ContextualComponent<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    render() {
        const { record } = this.props;
        const file = new File(record.value);

        return (
            <View style={this.theme.pageTheme}>
                <VideoPlayer file={file} onEnd={this.props.onEnd} onError={this.props.onError}/>
                <View style={this.theme.buttonsTopRightTheme}>
                    <IconButton style={{ backgroundColor: 'transparent' }} onPress={this.props.onQuit} icon={faTimes}/>
                </View>
            </View>
        );
    }
}
