// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { View } from 'react-native';

export class RecordButton extends ContextualComponent {
    render() {
        const { outerContainer, container, innerContainer } = this.theme.recordButtonTheme;
        return (
            <View pointerEvents="none" style={container}>
                <View style={outerContainer}>
                    <View style={innerContainer}/>
                </View>
            </View>
        );
    }
}
