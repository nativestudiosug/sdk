// @flow
import { ContextualComponent, File } from '@nativestudios/creace-core';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import { Countdown } from '@nativestudios/creace-ui';
import React from 'react';
import { ActivityIndicator, Platform, SafeAreaView, Text, TouchableWithoutFeedback, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import Video from 'react-native-video';
import { CameraSwitchButton } from './CameraSwitchButton';
import { RecordButton } from './RecordButton';
import { VideoProgressBar } from './VideoProgressBar';

type Props = {
    instruction: Instruction;
    onValue: Function;
    onError: Function;
    showCountdown?: boolean;
    cameraType?: RNCamera.Constants.Type;
    onRecordStarted?: Function;
    onRecordFinished?: Function;
    onCountdownStarted?: Function;
    onCountdownStopped?: Function;
    onCameraTypeChanged?: Function;
};

type State = {
    cameraType: RNCamera.Constants.Type;
    cameraReady: boolean;
    isRecording: boolean;
    showCountdown: boolean;
    recordStarted: number;
    value?: string;
};

export class VideoInstruction extends ContextualComponent<Props, State> {
    static defaultProps: Props = {
        showCountdown: true,
    };

    camera: ?RNCamera;

    constructor(props: Props) {
        super(props);

        this.state = {
            cameraType: undefined,
            cameraReady: false,
            isRecording: false,
            showCountdown: false,
            recordStarted: 0,
        };
    }

    getCameraQuality() {
        const { cameraQuality } = this.props.instruction;
        const { VideoQuality } = RNCamera.Constants;

        if (typeof cameraQuality !== 'undefined' && typeof VideoQuality[cameraQuality] === 'undefined') {
            return VideoQuality[cameraQuality];
        }

        return VideoQuality['720p'];
    }

    getCameraType() {
        if (this.props.cameraType !== undefined) {
            return this.props.cameraType;
        }

        if (this.state.cameraType !== undefined) {
            return this.state.cameraType;
        }

        if (this.props.instruction.camera === 'front') {
            return RNCamera.Constants.Type.front;
        }

        return RNCamera.Constants.Type.back;
    }

    async startRecording() {
        if (!this.camera || this.state.isRecording === true) {
            return;
        }
        this.setState({
            isRecording: true,
        });
        this.props.onRecordStarted && this.props.onRecordStarted();

        const { instruction } = this.props;
        let recordStarted;

        try {
            recordStarted = Date.now();
            this.setState({
                recordStarted,
            });
            const { uri } = await this.camera.recordAsync(Platform.select({
                ios: {
                    maxDuration: instruction.length,
                    orientation: 'landscapeRight',
                },
                android: {
                    maxDuration: instruction.length,
                    orientation: 'landscapeLeft',
                    quality: this.getCameraQuality(),
                },
            }));
            this.setState({
                isRecording: false,
            });
            const recordTime = (Date.now() - recordStarted) / 1000;
            if (recordTime >= Math.max(1, instruction.minlength)) {
                this.setState({ value: uri });
            }
        } catch (error) {
            this.errorHandler.notifyError(error);
        }
    }

    stopRecording() {
        if (!this.camera || this.state.isRecording === false) {
            return;
        }

        const { instruction } = this.props;
        const recordTime = (Date.now() - this.state.recordStarted) / 1000;

        if (recordTime < 1) {
            return;
        }

        if (recordTime < instruction.minlength) {
            return window.setTimeout(this.stopRecording.bind(this), 100);
        }

        this.camera.stopRecording();
    }

    render() {
        return (
            <View style={[this.theme.videoInstructionTheme.container]}>
                {this.renderVideo()}
                {this.renderCamera()}
                {this.renderCameraElements()}
            </View>
        );
    }

    renderVideo() {
        const { value } = this.state;

        if (!value) {
            return <View/>;
        }

        return <Video paused={true}
                      source={{ uri: value }}
                      onError={this.props.onError}
                      onLoad={this.onVideoLoad.bind(this)}/>;
    }

    renderCamera() {
        return <RNCamera
            captureAudio={true}
            defaultVideoQuality={this.getCameraQuality()}
            pictureSize={'1280x720'}
            playSoundOnCapture={true}
            type={this.getCameraType()}
            ref={(camera) => this.camera = camera}
            onCameraReady={this.onCameraReady.bind(this)}
            style={[this.theme.videoInstructionTheme.container]}/>;
    }

    renderCameraElements() {
        if (!this.state.cameraReady) {
            return (
                <View style={[this.theme.videoInstructionTheme.container, {
                    justifyContent: 'center',
                    alignItems: 'center',
                }]}>
                    <ActivityIndicator color={this.theme.colors.primary}/>
                </View>
            );
        }

        return (
            <View style={this.theme.videoInstructionTheme.container}>
                {this.renderInstruction()}
                {this.renderCameraChangeButton()}
                {this.renderProgressBar()}
                {this.renderCountdown()}
            </View>
        );
    }

    renderInstruction() {
        const backgroundColor = this.state.isRecording || this.state.showCountdown ? 'transparent' : 'rgba(0, 0, 0, 0.6)';
        const { containerHotZone, instructionContainer } = this.theme.videoInstructionTheme;

        return (
            <SafeAreaView
                style={[instructionContainer, { backgroundColor }]}>
                {this.renderInstructionText()}
                <RecordButton/>
                <TouchableWithoutFeedback
                    onPressIn={this.onPressIn.bind(this)}
                    onPressOut={this.onPressOut.bind(this)}>
                    <View style={[containerHotZone]}/>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        );
    }

    renderInstructionText() {
        if (this.state.isRecording || this.state.showCountdown) {
            return <View/>;
        }

        const { instruction } = this.props;

        return (
            <Text style={this.theme.videoInstructionTheme.instructionText}>
                {instruction.instruction_text}
            </Text>
        );
    }

    renderCameraChangeButton() {
        if (this.state.isRecording || this.state.showCountdown) {
            return <View/>;
        }

        return <CameraSwitchButton onPress={this.onCameraChangeButtonPress.bind(this)}/>;
    }

    renderProgressBar() {
        const { length, minlength } = this.props.instruction;
        return <VideoProgressBar duration={length}
                                 minDuration={minlength}
                                 showProgress={this.state.isRecording}/>;
    }

    renderCountdown() {
        if (!this.state.showCountdown) {
            return <View/>;
        }

        return <Countdown countdown={3} onDone={() => {
            this.setState({
                showCountdown: false,
            });
            this.props.onCountdownStopped && this.props.onCountdownStopped();
            this.startRecording();
        }}/>;
    }

    onCameraReady() {
        return this.setState({
            cameraReady: true,
        });
    }

    async onPressIn() {
        if (!this.state.cameraReady || this.state.showCountdown || this.state.isRecording) {
            return;
        }

        if (!this.props.showCountdown) {
            this.startRecording();

            return;
        }

        this.setState({
            showCountdown: true,
        });
        this.props.onCountdownStarted && this.props.onCountdownStarted();
    }

    onPressOut() {
        if (this.state.showCountdown) {
            return;
        }
        this.stopRecording();
    }

    onCameraChangeButtonPress() {
        const { front, back } = RNCamera.Constants.Type;
        const cameraType = this.state.cameraType === front ?
            back : front;

        this.setState({ cameraType });
        this.props.onCameraTypeChanged && this.props.onCameraTypeChanged(cameraType);
    }

    onVideoLoad() {
        this.props.onRecordFinished && this.props.onRecordFinished();
        this.props.onValue(this.state.value);
        this.setState({ value: null });
    }
}
