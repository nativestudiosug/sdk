// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { Text, TouchableHighlight } from 'react-native';

type Props = {
    onPress: Function;
};

type State = {};

export class RedoButton extends ContextualComponent<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    render() {
        const { container, text } = this.theme.redoButtonTheme;

        return (
            <TouchableHighlight style={[container]}
                                onPress={this.props.onPress}>
                <Text style={[text]}>{this.translate('redo.button.text')}</Text>
            </TouchableHighlight>
        );
    }
}
