// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { Animated, Dimensions, Easing, View } from 'react-native';

type Props = {
    showProgress: boolean;
    duration: number;
    minDuration: number;
};

export class VideoProgressBar extends ContextualComponent<Props> {
    constructor(props: Props) {
        super(props);

        this.state = {
            barHeight: new Animated.Value(0),
            minBarHeight: new Animated.Value(0),
        };
    }

    animate() {
        const { duration, minDuration, showProgress } = this.props;
        const { height } = Dimensions.get('screen');

        this.state.barHeight.setValue(0);
        this.state.minBarHeight.setValue(0);

        if (!showProgress) {
            Animated.timing(this.state.barHeight, {
                toValue: 0,
                duration: 0,
            }).start();
            Animated.timing(this.state.minBarHeight, {
                toValue: 0,
                duration: 0,
            }).start();

            return;
        }

        Animated.timing(this.state.barHeight, {
            toValue: height,
            duration: duration * 1000,
            easing: Easing.linear,
        }).start();
        Animated.timing(this.state.minBarHeight, {
            toValue: height * minDuration / duration,
            duration: minDuration * 1000 - 100,
            easing: Easing.linear,
        }).start();
    }

    render() {
        const { container, progressBar } = this.theme.videoProgressBarTheme;
        const { warning, primary } = this.theme.colors;
        this.animate();

        return (
            <View style={[container]}>
                <Animated.View
                    style={[progressBar, { height: this.state.barHeight, backgroundColor: primary }]}/>
                <Animated.View style={[progressBar, {
                    height: this.state.minBarHeight,
                    backgroundColor: warning,
                }]}/>
            </View>
        );
    }
}
