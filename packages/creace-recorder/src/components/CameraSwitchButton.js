// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import React from 'react';
import { TouchableHighlight, View } from 'react-native';
import Svg, { Circle, Path } from 'react-native-svg';

type Props = {
    onPress: Function;
};

export class CameraSwitchButton extends ContextualComponent<Props> {
    render() {
        const { container, button } = this.theme.cameraSwitchButtonTheme;

        return (
            <View style={container}>
                <TouchableHighlight style={[button]}
                                    onPress={this.props.onPress}>
                    <Svg width={30} height={30} viewBox="0 0 512 512"
                         fill={'none'}
                         stroke={this.theme.colors.light}
                         strokeWidth={14}
                         strokeLinecap={'round'}
                         strokeMiterlimit={10}>
                        <Path
                            d={'M374.63 145.69h-64.05l-3.27-12.32c-1.36-5.13-6.01-8.7-11.32-8.7h-83.3c-5.31 0-9.95 3.57-11.32 8.7l-3.27 12.32h-64.05c-5.43 0-9.83 4.4-9.83 9.83v127.22c0 5.43 4.4 9.83 9.83 9.83h240.58c5.43 0 9.83-4.4 9.83-9.83V155.53c0-5.44-4.4-9.84-9.83-9.84z'}/>
                        <Path
                            d={'M215.85 367.86C110.63 360.74 30.8 326.14 30.8 284.54c0-25.24 29.38-47.9 76-63.41M404.47 220.89c47.03 15.52 76.73 38.28 76.73 63.65 0 41.3-78.68 75.69-182.77 83.16'}
                            strokeLinejoin={'round'}/>
                        <Path d={'M192.5 339.89l28.86 29.99-36.94 19.46'}/>
                        <Circle cx="254.34" cy="220.89" r="43.18"/>
                    </Svg>
                </TouchableHighlight>
            </View>
        );
    }
}
