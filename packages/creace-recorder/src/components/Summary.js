// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import { Button } from '@nativestudios/creace-ui';
import React from 'react';
import { SafeAreaView, View } from 'react-native';
import Markdown from 'react-native-markdown-renderer';

type Props = {
    template: Template;
    onButtonProcessPress: Function;
    onButtonRecordsPress: Function;
};

export class Summary extends ContextualComponent<Props> {
    render() {
        const { onButtonRecordsPress, onButtonProcessPress, template } = this.props;
        const { safeArea, container, markdownContainer, button, buttonsContainer, markdown } = this.theme.summaryTheme;

        return (
            <SafeAreaView style={[safeArea]}>
                <View style={[container]}>
                    <View style={markdownContainer}>
                        <Markdown style={markdown}>
                            {template.processing_screen}
                        </Markdown>
                    </View>
                    <View style={[buttonsContainer]}>
                        <Button style={[button]} text={template.processing_button || this.translate('summary.button.process')} onPress={onButtonProcessPress}/>
                        <Button style={[button]} text={this.translate('summary.button.records')} onPress={onButtonRecordsPress}/>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}
