// @flow
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import React from 'react';
import { Text, View } from 'react-native';

type Props = {
    instruction: Instruction;
    type: 'upcoming' | 'current' | 'done';
};

export class InstructionsBarItem extends ContextualComponent<Props> {
    backgroundColors = {
        upcoming: 'white',
        current: this.theme.colors.primary,
        done: this.theme.colors.secondary,
    };

    render() {
        const { instruction } = this.props;
        const backgroundColor = this.backgroundColors[this.props.type];

        const { bar, label, container, position } = this.theme.instructionBarItemTheme;
        return (
            <View style={[container]}>
                <Text style={[label]}>{this.getLabel().toUpperCase()}</Text>
                <Text style={[position]}>{instruction.position}</Text>
                <View style={[bar, { backgroundColor }]}/>
            </View>
        );
    }

    getLabel(): string {
        switch (this.props.instruction.type) {
            case 'video':
                return this.translate('instruction.bar.item.video');
            case 'text':
                return this.translate('instruction.bar.item.text');
            case 'summary':
                return this.translate('instruction.bar.item.summary');
        }

        return '';
    }
}
