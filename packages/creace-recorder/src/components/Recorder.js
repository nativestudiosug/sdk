// @flow
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { ContextualComponent } from '@nativestudios/creace-core';
import type { Instruction } from '@nativestudios/creace-core/typings/Instruction';
import type { Template } from '@nativestudios/creace-core/typings/Template';
import { IconButton } from '@nativestudios/creace-ui';
import React from 'react';
import { Alert, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { InstructionsBar } from './InstructionsBar';
import { QuitQuestion } from './QuitQuestion';
import { RedoButton } from './RedoButton';
import { Summary } from './Summary';
import { TextInstruction } from './TextInstruction';
import { VideoInstruction } from './VideoInstruction';

type Props = {
    template: Template;
    showCountdown?: boolean;
    currentInstruction?: number;
    cameraType?: RNCamera.Constants.Type;
    onCameraTypeChanged?: Function;
    onRecord: Function;
    onQuit: Function;
    onButtonProcessPress: Function;
    onButtonRecordsPress: Function;
    onRedo?: Function;
    onError?: Function;
};

type State = {
    currentInstruction: number;
    didRedo: boolean;
    isRecording: boolean;
    wantsToQuit: boolean;
};

export class Recorder extends ContextualComponent<Props, State> {
    static defaultProps: Props = {
        showCountdown: true,
    };

    constructor(props: Props) {
        super(props);

        this.state = {
            currentInstruction: this.props.currentInstruction || 0,
            didRedo: false,
            isRecording: false,
            wantsToQuit: false,
        };
    }

    getCurrentInstruction(): Instruction {
        const { template } = this.props;

        return template.instructions[this.state.currentInstruction];
    }

    canRedo() {
        return this.state.currentInstruction > 0 && !this.state.isRecording;
    }

    redo() {
        if (this.canRedo()) {
            this.setState({
                currentInstruction: --this.state.currentInstruction,
                didRedo: true,
            });

            this.props.onRedo && this.props.onRedo(this.getCurrentInstruction());
        }
    }

    render() {
        return (
            <View style={[this.theme.pageTheme]}>
                {this.renderInstruction()}
                {this.renderInstructionsBar()}
                <View style={this.theme.buttonsTopRightTheme}>
                    {this.renderQuitButton()}
                </View>
                {this.renderQuitQuestion()}
                {this.renderRedoButton()}
            </View>
        );
    }

    renderInstruction() {
        const instruction: Instruction = this.getCurrentInstruction();
        const { onButtonRecordsPress, cameraType, template, showCountdown, onButtonProcessPress, onCameraTypeChanged } = this.props;

        if (!instruction) {
            return <Summary template={template}
                            onButtonProcessPress={onButtonProcessPress}
                            onButtonRecordsPress={onButtonRecordsPress}/>;
        }

        if (instruction.type === 'text') {
            return <TextInstruction instruction={instruction}
                                    onValue={this.onValue.bind(this)}/>;
        }

        return <VideoInstruction instruction={instruction}
                                 cameraType={cameraType}
                                 showCountdown={showCountdown}
                                 onCameraTypeChanged={onCameraTypeChanged}
                                 onCountdownStarted={() => this.setState({ isRecording: true })}
                                 onCountdownStopped={() => this.setState({ isRecording: true })}
                                 onRecordStarted={() => this.setState({ isRecording: true })}
                                 onRecordFinished={() => this.setState({ isRecording: false })}
                                 onError={this.onError.bind(this)}
                                 onValue={this.onValue.bind(this)}/>;
    }

    renderInstructionsBar() {
        if (this.state.isRecording) {
            return <View/>;
        }

        return <InstructionsBar instructions={this.props.template.instructions}
                                currentInstruction={this.state.currentInstruction}/>;
    }

    renderQuitButton() {
        if (this.state.isRecording) {
            return <View/>;
        }

        return <IconButton onPress={() => this.setState({ wantsToQuit: true })}
                           style={{ backgroundColor: 'transparent' }}
                           icon={faTimes}/>;
    }

    renderQuitQuestion() {
        if (!this.state.wantsToQuit) {
            return <View/>;
        }

        return <QuitQuestion onAbort={() => this.setState({ wantsToQuit: false })}
                             onAccept={this.props.onQuit}/>;
    }

    renderRedoButton() {
        if (!this.canRedo()) {
            return <View/>;
        }

        return <RedoButton onPress={this.redo.bind(this)}/>;
    }

    onError() {
        this.props.onError(this.getCurrentInstruction());
        Alert.alert(
            this.translate('record.error.alert.title'),
            this.translate('record.error.alert.message'),
        );
    }

    onValue(value: string) {
        const instruction = this.getCurrentInstruction();

        this.props.onRecord(instruction, value);

        this.setState({
            currentInstruction: ++this.state.currentInstruction,
            didRedo: false,
        });
    }
}
