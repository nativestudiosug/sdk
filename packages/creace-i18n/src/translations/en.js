module.exports = {
    locale: {
        de: 'German',
        en: 'English',
    },
    settings: {
        labels: {
            language: 'Language:',
            countdown: 'Countdown:',
        },
        values: {
            countdown: {
                enabled: 'Enabled',
                disabled: 'Disabled',
            }
        },
    },
    instruction: {
        bar: {
            item: {
                video: 'Scene',
                text: 'Text',
                summary: 'Send',
            },
        },
    },
    quit: {
        question: {
            text: 'Are you sure you want to cancel the recording?\nYou can continue later at this position.',
            button: {
                accept: 'Quit',
                abort: 'Cancel',
            },
        },
    },
    record: {
        error: {
            alert: {
                title: 'Record failed',
                message: 'The record failed, please try again.',
            },
        },
        list: {
            item: {
                video: {
                    button: {
                        add: 'Record',
                        edit: 'Redo',
                        view: 'View',
                    },
                },
                text: {
                    button: {
                        add: 'Add',
                        edit: 'Edit',
                    },
                },
            },
        },
    },
    summary: {
        button: {
            process: 'Process your video',
            records: 'View your records',
        },
    },
    video: {
        uploader: {
            alert: {
                offline: {
                    title: 'Offline',
                    message: 'You are currently offline, you can upload the files later.',
                },
                noWifi: {
                    title: 'No Wi-Fi connection',
                    message: 'Please check whether your signal is strong enough.\nIf you are unsure, just wait a bit and start the transfers later. If you press on ‘Continue’, the transfer starts immediately.',
                    button: {
                        quit: 'Cancel',
                        continue: 'Continue',
                    },
                },
            },
            state: {
                uploading: 'Uploading videos ...',
                uploaded: 'Videos uploaded ...',
                queued: 'Wating in the queue ...',
                rendering: 'Rendering video ...',
                failed: 'Rendering failed ... :-(',
                finished: 'Video rendered, downloading ...',
            },
        },
        view: {
            error: {
                downloadFailed: 'Downloading the rendered video failed.',
                duplicateFailed: 'Error occurred, please contact our support.',
            },
            action: {
                edit: 'Edit',
                delete: 'Delete',
            },
            sharing: {
                extra: {
                    title: 'Successfully shared.',
                },
            },
            save: {
                permissionRequest: {
                    title: 'Write permissions',
                    message: 'We need write permissions to save the video in your gallery.',
                },
                done: {
                    title: 'Video saved.',
                },
                error: {
                    title: 'Failed to save the video.',
                },
            },
        },
        list: {
            empty: 'You haven\'t created any videos, yet.',
            item: {
                button: {
                    view: 'View',
                    edit: 'Edit',
                    delete: 'Delete',
                    upload: 'Upload',
                },
            },
        },
        delete: {
            question: {
                text: 'Do you really want to delete this video?',
                switchLocal: 'Local video',
                switchRemote: 'Video in the cloud (Video is permanently deleted)',
                button: {
                    accept: 'Delete',
                    abort: 'Cancel',
                },
            }
        },
    },
    template: {
        detail: {
            button: {
                start: 'Create your video',
                continue: 'Continue with a video',
            },
        },
    },
    redo: {
        button: {
            text: 'Redo scene',
        },
    },
    modal: {
        cancel: {
            text: 'Close',
        },
    },
};
