module.exports = {
    locale: {
        de: 'Deutsch',
        en: 'Englisch',
    },
    settings: {
        labels: {
            language: 'Sprache:',
            countdown: 'Countdown:',
        },
        values: {
            countdown: {
                enabled: 'Aktiviert',
                disabled: 'Deaktiviert',
            }
        },
    },
    instruction: {
        bar: {
            item: {
                video: 'Szene',
                text: 'Text',
                summary: 'Senden',
            },
        },
    },
    quit: {
        question: {
            text: 'Willst Du die Aufnahme wirklich beenden?\nDu kannst später an der Stelle weitermachen.',
            button: {
                accept: 'Beenden',
                abort: 'Abbrechen',
            },
        },
    },
    record: {
        error: {
            alert: {
                title: 'Aufnahme fehlgeschlagen',
                message: 'Die Aufnahme ist leider fehlgeschlagen, versuche es doch bitte nochmal.',
            },
        },
        list: {
            item: {
                video: {
                    button: {
                        add: 'Aufnehmen',
                        edit: 'Neu aufnehmen',
                        view: 'Ansehen',
                    },
                },
                text: {
                    button: {
                        add: 'Hinzufügen',
                        edit: 'Bearbeiten',
                    },
                },
            },
        },
    },
    summary: {
        button: {
            process: 'Jetzt Video erstellen',
            records: 'Aufnahmen ansehen',
        },
    },
    video: {
        uploader: {
            alert: {
                offline: {
                    title: 'Offline',
                    message: 'Du bist derzeit offline, Du kannst es später nochmal probieren.',
                },
                noWifi: {
                    title: 'Keine W-Lan Verbindung',
                    message: 'Bitte überprüfe, ob deine Verbindung ausreichend gut ist.\\nBist Du unsicher, warte einfach und aktiviere die Übertragungen später. Drückst Du auf "Fortfahren", startet die Übertragung sofort.',
                    button: {
                        quit: 'Abbrechen',
                        continue: 'Fortfahren',
                    },
                },
            },
            state: {
                uploading: 'Videos werden hochgeladen ...',
                uploaded: 'Videos sind hochgeladen ...',
                queued: 'Video befindet sich in der Warteschlange ...',
                rendering: 'Video wird erstellt ...',
                failed: 'Erstellungen ist leider fehlgeschlagen ... :-(',
                finished: 'Video fertig, wird heruntergeladen ...',
            },
        },
        view: {
            error: {
                downloadFailed: 'Das fertige Video konnte nicht heruntergeladen werden.',
                duplicateFailed: 'Vorgang fehlgeschlagen, bitte wenden Sie sich an unseren Support.',
            },
            action: {
                edit: 'Bearbeiten',
                delete: 'Löschen',
            },
            sharing: {
                extra: {
                    title: 'Erfolgreich geteilt.',
                },
            },
            save: {
                permissionRequest: {
                    title: 'Schreib-Berechtigung',
                    message: 'Um das Video zu speichern, wird das Schreiben auf deinem Speicherplatz benötigt.',
                },
                done: {
                    title: 'Video wurde gespeichert.',
                },
                error: {
                    title: 'Video konnte nicht gespeichert werden.',
                },
            },
        },
        list: {
            empty: 'Du hast noch keine Videos erstellt.',
            item: {
                button: {
                    view: 'Ansehen',
                    edit: 'Bearbeiten',
                    delete: 'Löschen',
                    upload: 'Übertragen',
                },
            },
        },
        delete: {
            question: {
                text: 'Willst du wirklich das Video löschen?',
                switchLocal: 'Lokales Video',
                switchRemote: 'Video in der Cloud (Video wird unwiderruflich gelöscht)',
                button: {
                    accept: 'Löschen',
                    abort: 'Abbrechen',
                },
            }
        },
    },
    template: {
        detail: {
            button: {
                start: 'Erstelle Dein Video',
                continue: 'Fahre mit einem Video fort',
            },
        },
    },
    redo: {
        button: {
            text: 'Aufnahme wiederholen',
        },
    },
    modal: {
        cancel: {
            text: 'Schließen',
        },
    },
};
