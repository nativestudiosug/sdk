import * as de from './translations/de';
import * as en from './translations/en';
export * from './Translator';
export {
    de,
    en
}
