// @flow
import { Translator as CoreTranslator } from '@nativestudios/creace-core';
import I18n from 'i18n-js';
import * as RNLocalize from 'react-native-localize';
import type { Translations } from '../typings/Translations';

export const availableTranslations: Translations = {
    de: () => require('./translations/de'),
    en: () => require('./translations/en'),
};

export class Translator extends CoreTranslator {
    translations: Translations;

    get languages(): Array<string> {
        return Object.keys(this.translations);
    }

    constructor(translations?: Translations) {
        super();
        this.translations = translations || availableTranslations;
        this.setLanguage();
    }

    translate(text: string): string {
        const translations =  I18n.t(text);

        if (/^\[missing/.test(translations)) {
            console.log(translations);
            return text;
        }

        return translations;
    }

    setLanguage(language = null) {
        const fallbackLanguage = { languageTag: 'en' };
        const bestAvailableLanguage = RNLocalize.findBestAvailableLanguage(this.languages);

        if (!language || typeof this.translations[language] === 'undefined') {
            const { languageTag } = bestAvailableLanguage || fallbackLanguage;
            language = languageTag;
        }

        I18n.translations = { [language]: this.translations[language]() };
        I18n.locale = language;
    }
}
