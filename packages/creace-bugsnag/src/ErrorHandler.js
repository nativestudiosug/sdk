//@flow
import { ErrorHandler as CoreErrorHandler } from '@nativestudios/creace-core';
import { Client } from 'bugsnag-react-native';

export class ErrorHandler extends CoreErrorHandler {
    client: Client;

    constructor(id) {
        super();
        this.client = new Client(id)
    }

    notifyError(error: any) {
        super.notifyError(error);
        this.client.notify(error);
    }
}
