# CREACE SDK

React Native SDK to integrate CREACE functionality to your React Native app.

## Packages

- [Core](./packages/creace-core)
- [Bugsnag](./packages/creace-bugsnag)
- [Firebase](./packages/creace-firebase)
- [I18N](./packages/creace-i18n)
- [Recorder](./packages/creace-recorder)
- [Storage](./packages/creace-redux)
- [UI](./packages/creace-ui)
